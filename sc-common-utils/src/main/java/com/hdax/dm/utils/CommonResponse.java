package com.hdax.dm.utils;

import lombok.Data;

@Data
public class CommonResponse<T> {
    private String errorCode;// 响应 编码
    private String message;// 响应消息
    private T data; // 响应数据
    public CommonResponse(T data){
        this.errorCode = "0000";
        this.data = data;
    }

    public CommonResponse(String errorCode, T data){
        this.errorCode = errorCode;
        this.data = data;
    }

    public CommonResponse(String errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }
}
