package com.hdax.dm.pay.fegin;

import com.hdax.dm.api.OrderControllerApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(value = "dm-order")
@Component
public interface OrderFeign extends OrderControllerApi {
}
