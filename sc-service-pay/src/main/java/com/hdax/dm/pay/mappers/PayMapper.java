package com.hdax.dm.pay.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.pay.Trade;

public interface PayMapper extends BaseMapper<Trade> {
}
