package com.hdax.dm.pay.fegin;

import com.hdax.dm.api.SchedulerSeatControllerApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(value = "dm-scheduler")
@Component
public interface ScheduleSeatFeign extends SchedulerSeatControllerApi {
}
