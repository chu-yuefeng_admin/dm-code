package com.hdax.dm.pay.service.Impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.order.Order;
import com.hdax.dm.entity.pay.Trade;
import com.hdax.dm.pay.config.AlipayConfig;
import com.hdax.dm.pay.fegin.OrderFeign;
import com.hdax.dm.pay.fegin.ScheduleSeatFeign;
import com.hdax.dm.pay.mappers.PayMapper;
import com.hdax.dm.pay.service.PayService;
import com.hdax.dm.pay.vo.TradeInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayServiceImpl extends ServiceImpl<PayMapper,Trade> implements PayService {
    @Autowired
    private AlipayConfig alipayConfig;
    @Autowired
    private OrderFeign orderFeign;
    @Autowired
    private ScheduleSeatFeign seatFeign;
    @Override
    public String aliPay(String orderNo) throws AlipayApiException {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(alipayConfig.getGatewayUrl(),
                alipayConfig.getAppId(), alipayConfig.getPrivateKey(), "json",
                alipayConfig.getCharset(), alipayConfig.getPublicKey(), alipayConfig.getSignType());
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//设置请求参数
        alipayRequest.setReturnUrl(alipayConfig.getReturnUrl());
        alipayRequest.setNotifyUrl(alipayConfig.getNotifyUrl());
        Order order = orderFeign.detail(orderNo);
        alipayRequest.setBizContent("{\"out_trade_no\":\""+ orderNo +"\","
                + "\"total_amount\":\""+ order.getTotalAmount()+"\","
                + "\"subject\":\""+ order.getItemName() +"\","
                + "\"body\":\""+ "" +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //真正请求
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println(result);
        return result;
    }

    /**
     * 支付宝异步走到这里 代表成功支付  然后我们修改订单 状态为2 已支付  并且修改座位为  已出售
     * @param vo
     * @return
     */
    @Override
    public String payed(TradeInfoVo vo) {
        //根据订单好查询订单支付状态
        QueryWrapper<Trade> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orderNo",vo.getOrderNo());
        Trade trade = getOne(queryWrapper);
        //如果查询到信息就说明此订单已经完成了  一系列的操作直接返回
        if(trade!=null) return "";
        Trade trade1 = new Trade();
        BeanUtils.copyProperties(vo,trade1);
        System.out.println("sadasdasdasdasdsadasdas");
        System.out.println(trade1);
        trade1.setId(vo.getTradeNo());
        save(trade1);
        //修改订单操作 将订单状态修改为 2已支付
        orderFeign.upOrderStateSuccess(vo.getOrderNo(),vo.getTradeNo());
        //修改座位状态为已出售
        seatFeign.UpThreeSchedulerSeat(vo.getOrderNo());
        return vo.getTradeStatus();
    }
}
