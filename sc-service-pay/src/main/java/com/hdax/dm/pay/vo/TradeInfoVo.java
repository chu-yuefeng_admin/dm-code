package com.hdax.dm.pay.vo;

import lombok.Data;

import java.util.Date;

@Data
public class TradeInfoVo {
    //支付宝订单号
    private String tradeNo;
    //商家订单号
    private String orderNo;
    //买家账号
    private String buyerAccount;
    //交易状态
    private String tradeStatus;
    //实收金额
    private Double receiptAmount;
    //买家支付金额
    private Double buyerPayAmount;
    //交易时间
    private Date gmtPayment;
}
