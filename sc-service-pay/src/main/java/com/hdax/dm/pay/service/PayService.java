package com.hdax.dm.pay.service;

import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.pay.Trade;
import com.hdax.dm.pay.vo.TradeInfoVo;

public interface PayService extends IService<Trade> {
    /**
     * 根据订单号 支付信息的返回
     * @param orderNo
     * @return
     * @throws AlipayApiException
     */
    String aliPay(String orderNo) throws AlipayApiException;

    String payed(TradeInfoVo vo);
}
