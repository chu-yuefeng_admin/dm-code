package com.hdax.dm.scheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//排期Dto
public class TimePlanDto {
    private Long id;//排期Id
    private String title;//排期名称
    private Long itemId;//商品Id
    private LocalDateTime  startTime;//开始日期
    private LocalDateTime endTime;//结束日期
    private Long cinemaId;//剧场ID
}
