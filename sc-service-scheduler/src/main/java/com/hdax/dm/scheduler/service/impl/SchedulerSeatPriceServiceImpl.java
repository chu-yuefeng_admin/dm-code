package com.hdax.dm.scheduler.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.scheduler.SchedulerSeat;
import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;
import com.hdax.dm.scheduler.dto.PriceDto;
import com.hdax.dm.scheduler.feign.OrderFeign;
import com.hdax.dm.scheduler.mappers.SchedulerSeatMapper;
import com.hdax.dm.scheduler.mappers.SchedulerSeatPriceMapper;
import com.hdax.dm.scheduler.service.SchedulerSeatPriceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public  class SchedulerSeatPriceServiceImpl extends ServiceImpl<SchedulerSeatPriceMapper, SchedulerSeatPrice> implements SchedulerSeatPriceService {
    @Autowired
    private SchedulerSeatMapper schedulerSeatMapper;

    /**
     * 商品详情页的每个票期的价格
     * @param scheduleId
     * @return
     */
    @Override
    public List<PriceDto> getPrice(Long scheduleId) {
        QueryWrapper<SchedulerSeatPrice> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("scheduleId",scheduleId);
        List<PriceDto> priceDtos = new ArrayList<>();
        list(queryWrapper).forEach(schedulerSeatPrice -> {
            PriceDto priceDto = new PriceDto();
            BeanUtils.copyProperties(schedulerSeatPrice,priceDto);
            //查询排期座位表中记录的订单冗余信息 判断 出来 该日期的购买数量是否大与30
            QueryWrapper<SchedulerSeat>  seatQueryWrapper = new QueryWrapper<>();
            seatQueryWrapper.eq("scheduleId",scheduleId)
                            .isNotNull("orderNo")//订单编号
                            .isNotNull("userId")//如果已经售出记录购买用户id
                            .ne("status",1);//座位状态(0:没座位 1:有座位 2:锁定待付款 3:已售出 )
            priceDto.setIsHaveSeat((short) (schedulerSeatMapper.selectCount(seatQueryWrapper)>30?0:1));
            priceDtos.add(priceDto);
        });
        return priceDtos;
    }
}
