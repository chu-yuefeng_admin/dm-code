package com.hdax.dm.scheduler.controller;

import com.hdax.dm.scheduler.dto.TimePlanDto;
import com.hdax.dm.scheduler.service.SchedulerService;
import com.hdax.dm.utils.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/scheduler")
public class TimePlanController {

    @Autowired
    private SchedulerService schedulerService;

    @PostMapping(path = "desc/getTimePlan")
    public CommonResponse<List<TimePlanDto>> getTimePlan(@RequestBody Map<String,Long> params){
        Long itemId = params.get("itemId");//商品编号
        return schedulerService.getTimePlan(itemId);
    }
}
