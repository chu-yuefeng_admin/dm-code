package com.hdax.dm.scheduler.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.scheduler.Scheduler;
import com.hdax.dm.entity.scheduler.SchedulerSeat;
import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;
import com.hdax.dm.scheduler.dto.*;
import com.hdax.dm.scheduler.mappers.SchedulerSeatMapper;
import com.hdax.dm.scheduler.mappers.SchedulerSeatPriceMapper;
import com.hdax.dm.scheduler.service.ScheduleStateService;
import com.hdax.dm.scheduler.service.SchedulerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ScheduleStateServiceImpl extends ServiceImpl<SchedulerSeatMapper, SchedulerSeat> implements ScheduleStateService {
    /**
     * 公共接口
     * @param scheduleId
     * @param isNotAll
     * @return
     */
    @Override
    public List<SchedulerSeat> seats(Long scheduleId, boolean isNotAll) {
        List<SchedulerSeat> list = baseMapper.seats(scheduleId,isNotAll);
        return list;
    }
    @Autowired
    private SchedulerService schedulerService;//节目排期
    @Autowired
    private SchedulerSeatPriceMapper schedulerSeatPriceMapper;

    /**
     * 公共方法 用来把 数字 换成字母
     * @param AreaLevel
     * @return
     */
    public String AreaLevelName (Long AreaLevel){
        String AreaLevelName = "";
        if(AreaLevel==1) AreaLevelName="A";
        if(AreaLevel==2) AreaLevelName="B";
        if(AreaLevel==3) AreaLevelName="C";
        return AreaLevelName;
    }
    /**
     * 剧场座位页面 剧场状态业务逻辑实现
     * @param scheduleId
     * @param //cinemaId
     * @return
     */
    @Override
    public ScheduleDto getScheduleState(Long scheduleId) {
        List<ScheduleDto.SeatInfo> seatInfos = new ArrayList<>();
        List<ScheduleDto.SeatPrice> seatPrices = new ArrayList<>();
        //查询排期的详情
        Scheduler scheduler = schedulerService.scheduler(scheduleId);
        ScheduleDto scheduleDto = new ScheduleDto();//剧场座位信息Dto
        scheduleDto.setScheduleId(scheduler.getId());
        scheduleDto.setCinemaId(scheduler.getCinemaId());
        seats(scheduleId,true).forEach(schedulerSeat -> {
            ScheduleDto.SeatInfo seatInfo =new ScheduleDto.SeatInfo();
            BeanUtils.copyProperties(schedulerSeat,seatInfo);
            seatInfo.setAreaLevel(AreaLevelName(schedulerSeat.getAreaLevel()));
            seatInfos.add(seatInfo);
        });
        QueryWrapper<SchedulerSeatPrice> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("scheduleId",scheduleId);
        schedulerSeatPriceMapper.selectList(queryWrapper).forEach(schedulerSeatPrice -> {
            ScheduleDto.SeatPrice seatPrice = new ScheduleDto.SeatPrice();
            seatPrice.setPrice(schedulerSeatPrice.getPrice());
            seatPrice.setAreaLevelName(AreaLevelName(schedulerSeatPrice.getAreaLevel()));
            seatPrices.add(seatPrice);
        });
        scheduleDto.setSeatInfoList(seatInfos);
        scheduleDto.setSeatPriceList(seatPrices);
        return scheduleDto;
    }

    /**
     * 剧场页面 每个票期的剧场
     * @param scheduleId
     * @param cinemaId
     * @return
     */
    @Override
    public SeatListDto getSeatList(Long scheduleId, Long cinemaId) {
        SeatListDto seatListDto = new SeatListDto();
        seatListDto.setCinemaId(cinemaId);
        //select * from dm_scheduler_seat group by x;
        // 1: 1 2 3 4
        // 2: 1 2 3 4
        // 3: 1 2 3 4
        QueryWrapper<SchedulerSeat> seatQueryWrapper = new QueryWrapper<>();
        seatQueryWrapper.eq("scheduleId",scheduleId);
        //java8 stream groupby
        Map<Long,List<SchedulerSeat>> map =list(seatQueryWrapper).stream().collect(Collectors.groupingBy(SchedulerSeat::getX));
        List<String> seatArray = new ArrayList<>();
        map.forEach((key,value)->{
            //每一个value都是一个集合，包含了每一排中的每个座位
            //key x 一排排座位

            //buffer存储了每一排座位的每一个字符表示
            //_ 没座位  areaLevel 1:a 2:b 3:c
            //___aaaaabbbbbccccc___
            StringBuffer buffer = new StringBuffer();
            value.forEach(seat->{
                System.out.println(seat);
                if (seat.getStatus() == 0) {
                    buffer.append("_");
                }else{
                    if (seat.getAreaLevel() == 0L) {
                        buffer.append("_");
                    } else if(seat.getAreaLevel() == 1L){
                        buffer.append("a");
                    } else if (seat.getAreaLevel() == 2L) {
                        buffer.append("b");
                    } else if (seat.getAreaLevel() == 3L) {
                        buffer.append("c");
                    }
                }
            });
            seatArray.add(buffer.toString());
        });
        SeatDto seatDto = new SeatDto();
        seatDto.setSeatArray(seatArray);
        seatListDto.setSeatArray(seatArray);
        return seatListDto;
    }
}
