package com.hdax.dm.scheduler.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.scheduler.Scheduler;
import com.hdax.dm.scheduler.dto.TimePlanDto;
import com.hdax.dm.utils.CommonResponse;

import java.util.List;

public interface SchedulerService extends IService<Scheduler> {
    /**
     * 查询某场演出的具体排期信息
     */
    Scheduler scheduler(Long id);
    /**
     * 获取演出时间排期
     */
    CommonResponse<List<TimePlanDto>> getTimePlan(Long itemId);
}
