package com.hdax.dm.scheduler.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.api.SchedulerSeatControllerApi;
import com.hdax.dm.entity.scheduler.SchedulerSeat;
import com.hdax.dm.scheduler.mappers.SchedulerMapper;
import com.hdax.dm.scheduler.mappers.SchedulerSeatMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Service
//提供外部访问 排期座位信息的实现方法
public class RestSchedulerSeatService extends ServiceImpl<SchedulerSeatMapper, SchedulerSeat> implements SchedulerSeatControllerApi {
    /**
     * 根据排期id 和X 轴 Y轴  查询 排期座位信息
     * @param schedulerId
     * @param X
     * @param Y
     * @return
     */
    @Override
    public SchedulerSeat getOneSchedulerSeat(Long schedulerId, Long X, Long Y){
        System.out.println(schedulerId);
        System.out.println(X);
        System.out.println(Y);
        QueryWrapper<SchedulerSeat> seatQueryWrapper = new QueryWrapper<>();
        seatQueryWrapper.eq("scheduleId",schedulerId);
        seatQueryWrapper.eq("x",X);
        seatQueryWrapper.eq("y",Y);
        return this.getOne(seatQueryWrapper);
    }
    /**
     * 通过排期id  和X轴 y轴修改 排期座位信息
     * @return
     */
    @Override
    public int UpOneSchedulerSeat(SchedulerSeat schedulerSeat) {
        return baseMapper.UpOneSchedulerSeat(schedulerSeat);
    }
    /**
     * 通过订单号 修改 排期座位信息  将座位改为 已占座
     * @return
     */
    @Override
    public boolean UpTwoSchedulerSeat(String orderNo) {
        boolean flag = false;
        if(baseMapper.UpTwoSchedulerSeat(orderNo)>0)flag = true;
        return flag;
    }

    @Override
    public boolean UpThreeSchedulerSeat(String orderNo) {
        boolean flag = false;
        System.out.println("sssssssssssssssssssssssss");
        System.out.println(orderNo);
        if(baseMapper.UpThreeSchedulerSeat(orderNo)>0)flag = true;
        return flag;
    }
}
