package com.hdax.dm.scheduler.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.scheduler.Scheduler;
import com.hdax.dm.scheduler.dto.TimePlanDto;
import com.hdax.dm.scheduler.feign.ImageFeign;
import com.hdax.dm.scheduler.mappers.SchedulerMapper;
import com.hdax.dm.scheduler.service.SchedulerService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SchedulerServiceImpl extends ServiceImpl<SchedulerMapper, Scheduler> implements SchedulerService {


    @Override
    public Scheduler scheduler(Long id) {
        Scheduler scheduler = getById(id);
        return scheduler;
    }
    /**
     * 每个商品的排期
     * @param itemId
     * @return
     */
    @Override
    public CommonResponse<List<TimePlanDto>> getTimePlan(Long itemId) {
        QueryWrapper<Scheduler> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("itemId",itemId);
        List<Scheduler> schedulers = list(queryWrapper);
        List<TimePlanDto> dtos = new ArrayList<>();
        schedulers.forEach(scheduler -> {
            TimePlanDto dto = new TimePlanDto();
            dto.setId(scheduler.getId());
            dto.setStartTime(scheduler.getStartTime());
            dto.setEndTime(scheduler.getEndTime());
            dto.setCinemaId(scheduler.getCinemaId());
            dto.setItemId(scheduler.getItemId());
            dto.setTitle(scheduler.getTitle());
            dtos.add(dto);
        });
        return ResponseUtil.returnSuccess(dtos);
    }
}
