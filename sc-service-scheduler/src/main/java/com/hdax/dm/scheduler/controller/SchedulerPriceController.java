package com.hdax.dm.scheduler.controller;

import com.hdax.dm.scheduler.dto.PriceDto;
import com.hdax.dm.scheduler.service.SchedulerSeatPriceService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/scheduler")
//每个票期的价格信息
public class SchedulerPriceController {
    @Autowired
    private SchedulerSeatPriceService priceService;

    @PostMapping(path = "desc/getPrice")
    public CommonResponse<List<PriceDto>> getPrice(@RequestBody Map<String,Long> params){
        Long scheduleId = params.get("scheduleId");
        return  ResponseUtil.returnSuccess(priceService.getPrice(scheduleId));
    }
}
