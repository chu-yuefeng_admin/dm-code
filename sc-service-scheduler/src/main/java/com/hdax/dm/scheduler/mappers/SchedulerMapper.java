package com.hdax.dm.scheduler.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.scheduler.Scheduler;

public interface SchedulerMapper extends BaseMapper<Scheduler> {
}
