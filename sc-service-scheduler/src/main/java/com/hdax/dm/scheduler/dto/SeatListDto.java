package com.hdax.dm.scheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//剧场的原始座位
public class SeatListDto {
    private Long cinemaId;
    private List<String> seatArray;
}
