package com.hdax.dm.scheduler.service;

import com.hdax.dm.entity.scheduler.SchedulerSeat;
import com.hdax.dm.scheduler.dto.ScheduleDto;
import com.hdax.dm.scheduler.dto.SeatListDto;

import java.util.List;


public interface ScheduleStateService{
    //剧场状态业务逻辑接口
    ScheduleDto getScheduleState(Long scheduleId);
    //用于查询剧场的原始座位
    SeatListDto getSeatList(Long scheduleId,Long cinemaId);
    /**
     * 根据isNotAll动态判断查询的座位是全部还是可购买
     * isNotAll : true  查询可购买
     * isNotAll : false 查询全部座位
     * @return
     */
    List<SchedulerSeat> seats(Long scheduleId, boolean isNotAll);//公共接口
}
