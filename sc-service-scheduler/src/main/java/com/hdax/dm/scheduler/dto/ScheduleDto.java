package com.hdax.dm.scheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//剧场座位状态相关属性
public class ScheduleDto {
    private Long scheduleId;//排期Id
    private Long cinemaId;//剧场id
    private List<SeatPrice> seatPriceList;
    private List<SeatInfo> seatInfoList;
    @Data
    public static class SeatPrice{//座位价格列表(不同区域的价格)
        private String areaLevelName; //座位区域级别名称(A,B,C)
        private Double price;//座位价格
    }
    @Data
    public static class SeatInfo{//座位信息列表(座位状态信息)
        private Long id;//座位Id
        private Long X;//
        private Long Y;//
        private String areaLevel;//区域级别(1:A;2:B;3:C)
        private Long cinemaId;//剧场Id
        private Long sort;//座位排序
        private Long status;//座位状态(0:有座位 1:座位已锁定 2：座位已售出)
        private Double price;//购买票金额
    }
}
