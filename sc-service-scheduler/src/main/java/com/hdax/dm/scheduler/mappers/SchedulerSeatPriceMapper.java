package com.hdax.dm.scheduler.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;

public interface SchedulerSeatPriceMapper extends BaseMapper<SchedulerSeatPrice> {
}
