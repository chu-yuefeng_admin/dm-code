package com.hdax.dm.scheduler.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.scheduler.SchedulerSeat;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SchedulerSeatMapper extends BaseMapper<SchedulerSeat> {
    @Select({
            "<script>",
            "select p.price,s.* from dm_scheduler_seat s,dm_scheduler_seat_price p",
            " where s.scheduleId = p.scheduleId and s.areaLevel = p.areaLevel",
            "<if test='isNotAll'>",
            " and s.orderNo is null and s.userId is null and s.status = 1",
            "</if>",
            " and s.scheduleId=#{scheduleId}",
            "</script>"
    })
    List<SchedulerSeat> seats(@Param("scheduleId")Long scheduleId, @Param("isNotAll")Boolean isNotAll);
    @Update({
            "update dm_scheduler_seat",
            " set orderNo = null,",
            " userId = null,",
            " `status` = 1",
            " where orderNo = #{orderNo}"
    })
    //通过订单号 修改 排期座位信息 将座位改为 已占座
    int UpTwoSchedulerSeat(@Param("orderNo")String orderNo);
    @Update({
            "update dm_scheduler_seat",
            " set orderNo = #{seat.orderNo},",
            " userId = #{seat.userId},",
            " status = 2",
            " where x=#{seat.x} and y=#{seat.y}",
            " and scheduleId=#{seat.scheduleId} and arealevel = #{seat.areaLevel}"
    })
    //通过排期id  和X轴 y轴修改 排期座位信息
    int UpOneSchedulerSeat(@Param("seat")SchedulerSeat seat);

    /**
     * 通过订单号修改其座位为  已出售 3
     * @param orderNo
     * @return
     */
    @Update({
            "update dm_scheduler_seat set",
            " `status` = 3",
            " where orderNo = #{orderNo}"
    })
    int UpThreeSchedulerSeat(@Param("orderNo")String orderNo);
}
