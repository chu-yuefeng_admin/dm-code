package com.hdax.dm.scheduler.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.api.SchedulerSeatPriceControllerApi;
import com.hdax.dm.entity.scheduler.SchedulerSeat;
import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;
import com.hdax.dm.scheduler.mappers.SchedulerSeatPriceMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

@Service
public class RestSchedulerSeatPriceService extends ServiceImpl<SchedulerSeatPriceMapper, SchedulerSeatPrice> implements SchedulerSeatPriceControllerApi {
    /**
     * 根据排期id 和 座位等级获取 座位价格信息
     * @param schedulerId
     * @param areaLevel
     * @return
     */
    @Override
    public SchedulerSeatPrice getOneSchedulerSeatPrice(Long schedulerId, Long areaLevel) {
        QueryWrapper<SchedulerSeatPrice> schedulerSeatPriceQueryWrapper = new QueryWrapper<>();
        schedulerSeatPriceQueryWrapper.eq("scheduleId",schedulerId);
        schedulerSeatPriceQueryWrapper.eq("areaLevel",areaLevel);
        return this.getOne(schedulerSeatPriceQueryWrapper);
    }
}
