package com.hdax.dm.scheduler.controller;

import com.hdax.dm.scheduler.dto.ScheduleDto;
import com.hdax.dm.scheduler.dto.SeatListDto;
import com.hdax.dm.scheduler.service.ScheduleStateService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(path = "/scheduler")
public class ScheduleStateController {
    @Autowired
    private ScheduleStateService scheduleStateService;

    /**
     *获取座位状态等信息
     * @param params
     * @return
     */
    @PostMapping(path = "p/choose/seat/getSchedule")
    public CommonResponse<ScheduleDto> getSchedule(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(scheduleStateService.getScheduleState(params.get("scheduleId")));
    }

    /**
     * 获取座位信息api_chooseSeat.getSchedule
     * @param params
     * @return
     */
    @PostMapping(path = "p/choose/seat/getSeatList")
    public CommonResponse<SeatListDto> getSeatList(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(scheduleStateService.getSeatList(params.get("scheduleId"),params.get("cinemaId")));
    }
}
