package com.hdax.dm.scheduler.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;
import com.hdax.dm.scheduler.dto.PriceDto;
import com.hdax.dm.utils.CommonResponse;

import java.util.List;

public interface SchedulerSeatPriceService extends IService<SchedulerSeatPrice> {

    /**
     * 根据排期id查询所有场次之内的各类票价
     */
    List<PriceDto> getPrice(Long scheduleId);
}
