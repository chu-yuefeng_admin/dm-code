package com.hdax.dm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//@EnableEurekaClient //注册中心客户端（eureka-server）
@EnableDiscoveryClient //注册中心客户端(通用)
@EnableFeignClients // 开启HTTP远程调用
@MapperScan(basePackages = "com.hdax.dm.scheduler.mappers")
public class MainApplication {
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class,args);
    }
}
