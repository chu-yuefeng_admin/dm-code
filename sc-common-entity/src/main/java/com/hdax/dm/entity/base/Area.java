package com.hdax.dm.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.Data;

@Data
@TableName(value = "dm_area")
//对应数据库的市区信息
public class Area extends BaseTimeEntity {
    @TableId
    private Long id;
    private String name;
    private String parent;
    private Long level;
    private Long isHot;
}
