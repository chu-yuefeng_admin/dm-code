package com.hdax.dm.entity.scheduler;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "dm_scheduler_seat_price")
public class SchedulerSeatPrice extends BaseTimeEntity {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Double price;
    private Long areaLevel;
    private Long scheduleId;
    private Long seatNum;
}
