package com.hdax.dm.entity.scheduler;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "dm_scheduler")
public class Scheduler extends BaseTimeEntity {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String title;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Long itemId;
    private Long cinemaId;
}
