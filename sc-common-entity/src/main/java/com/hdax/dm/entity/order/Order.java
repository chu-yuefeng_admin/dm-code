package com.hdax.dm.entity.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName(value = "dm_order")
public class Order {
  @TableId(type = IdType.AUTO)
  private Long id;//订单主键
  private String orderNo;//订单编号
  private Long userId;//下单用户Id
  private Long schedulerId;//排期ID
  private Long itemId;//剧集Id(冗余)
  private String itemName;//
  private String wxTradeNo;//微信交易号
  private String aliTradeNo;//支付宝交易号
  private Long orderType;//订单类型(0:未支付 -1:支付超时/支付取消 2:已支付)
  private String payType;//支付类型(1:微信支付 2：支付宝支付)
  private Long totalCount;//购买数目
  private Long area;//
  private Long sourceType;//0:WEB端 1:手机端 2:其他客户端
  private Long isNeedInvoice;//是否需要发票（0：不需要 1：需要）
  private Long invoiceType;//发票类型（0：个人 1：公司）
  private String invoiceHead;//发票抬头
  private String invoiceNo;//发票号
  private Long isNeedInsurance;//是否需要保险(0：不需要 1:需要)
  private Double totalAmount;//总价
  private Double insuranceAmount;//保险金额
  private LocalDateTime createdTime;//创建时间
  private LocalDateTime updatedTime;//更新时间
}
