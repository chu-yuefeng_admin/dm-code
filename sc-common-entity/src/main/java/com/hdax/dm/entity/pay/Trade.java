package com.hdax.dm.entity.pay;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "dm_trade")
public class Trade extends BaseTimeEntity {
    private String id;//
    //支付宝订单号
    private String tradeNo;
    //商家订单号
    private String orderNo;
    //买家账号
    private String buyerAccount;
    //交易状态
    private String tradeStatus;
    //实收金额
    private Double receiptAmount;
    //买家支付金额
    private Double buyerPayAmount;
    //交易时间
    private Date gmtPayment;
}
