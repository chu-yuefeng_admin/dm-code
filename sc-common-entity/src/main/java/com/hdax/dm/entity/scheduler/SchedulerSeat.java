package com.hdax.dm.entity.scheduler;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "dm_scheduler_seat")
public class SchedulerSeat extends BaseTimeEntity {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long x;
    private Long y;
    private Long areaLevel;
    private Long scheduleId;
    private String orderNo;
    private Long userId;
    private Long status;
    private Long sort;
}
