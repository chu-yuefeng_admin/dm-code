package com.hdax.dm.entity.item;

import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


//剧 评表
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName(value = "dm_item_comment")
public class ItemComment extends BaseTimeEntity {
  private Long id;//
  private Long itemId;//剧目Id
  private Long userId;//评论用户ID
  private Long score;//评分(1-10)
  private String content;//剧评
}
