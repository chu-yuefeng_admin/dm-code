package com.hdax.dm.entity.order;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName(value = "dm_order_link_user")
public class OrderLinkUser {
  @TableId(type = IdType.AUTO)
  private Long id;//
  private Long orderId;//订单id
  private Long linkUserId;//联系人id
  private String linkUserName;//入住人姓名逗号分隔
  private Long X;//
  private Long Y;//
  private Double price;//价格
  private LocalDateTime createdTime;
  private LocalDateTime updatedTime;
}
