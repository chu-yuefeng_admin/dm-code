package com.hdax.dm.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@TableName(value = "dm_user")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseTimeEntity{
    @TableId(type = IdType.AUTO)
    private Long id;
    private String phone;//手机号
    private String password;//密码
    private String wxUserId;//  wxid
    private String realName;//真实姓名
    private String nickName;//昵称
    private Long sex;//性别(0:男,1:女)
    private String hobby;//兴趣爱好
    private String idCard;//身份证号
    private LocalDateTime birthday;//生日


}
