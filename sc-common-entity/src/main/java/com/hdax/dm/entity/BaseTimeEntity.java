package com.hdax.dm.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

//通用创建和修改的时候  变化 的类
@Data
public class BaseTimeEntity implements Serializable {
    @TableField(fill = FieldFill.INSERT) //创建时自动填充
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)//创建与修改时自动填充
    private LocalDateTime updatedTime;
}
