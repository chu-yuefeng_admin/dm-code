package com.hdax.dm.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hdax.dm.entity.BaseTimeEntity;
import lombok.Data;

@Data
@TableName(value = "dm_image")
// 对应数据图片的类
public class Image extends BaseTimeEntity {
    @TableId
    private Long id;
    private String imgUrl;
    private Long targetId;
    private Long sort;
    private Long type;
    private Long category;
}
