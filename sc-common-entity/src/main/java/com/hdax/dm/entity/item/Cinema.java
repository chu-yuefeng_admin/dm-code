package com.hdax.dm.entity.item;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "dm_cinema")
//影院类
public class Cinema {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private String address;
    private Long areaId;
    private String areaName;
    private Long xLength;
    private Long latitude;
    private Long longitude;
    private String createdTime;
    private String updatedTime;
}
