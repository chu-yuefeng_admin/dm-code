package com.hdax.dm.api;

import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.entity.order.Order;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface OrderControllerApi{
    /**
     * 根据排期id获取orderd的购买数量
     * @param id
     * @return
     */
    @PostMapping(path = "/order/selectOrder")
    Long SelectOrder(@RequestParam("id")Long id);

    /**
     * 根据订单编号获取订单信息
     * @param orderNo
     * @return
     */
    @PostMapping(path = "/order/detail")
    Order detail(@RequestParam("orderNo")String orderNo);

    /**
     * 修改订单状态为成功支付和添加支付宝订单号
     * @param orderNo
     * @return
     */
    @PostMapping(path = "/order/upOrderStateSuccess")
    boolean upOrderStateSuccess(@RequestParam("orderNo")String orderNo,@RequestParam("aliTradeNo")String aliTradeNo);

    /**
     * 根据用户id  和 商品id  查询订单中  是否有该用户购买过
     * @param userId
     * @param itemId
     * @return
     */
    @PostMapping(path = "/order/getCount")
    Long getCount(@RequestParam("userId")Long userId,@RequestParam("itemId") Long itemId);
}
