package com.hdax.dm.api;

import com.hdax.dm.entity.user.LinkUser;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface LinkUserControllerApi {
    /**
     * 通过用户联系人id 和 用户id  获取联系人信息
     * @param id
     * @param uId
     * @return
     */
    @PostMapping(path = "/user/getOneLinkUser")
    LinkUser getOneLinkUser(@RequestParam("id")Long id, @RequestParam("uId")Long uId);
}
