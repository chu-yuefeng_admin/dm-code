package com.hdax.dm.api;

import com.hdax.dm.entity.item.ItemType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface ItemTypeControllerApi {
    /**
     * 根据id获取分类单个信息
     * @param id
     * @return
     */
    @PostMapping(path = "/rest/itemType")
    ItemType itemType(@RequestParam("id")Integer id);
}
