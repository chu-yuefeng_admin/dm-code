package com.hdax.dm.api;

import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface SchedulerSeatPriceControllerApi {
    /**
     * 通过 排期id 和 座位等级 获取排期价格信息
     * @param schedulerId
     * @param areaLevel
     * @return
     */
    @PostMapping(path = "scheduler/OneSchedulerSeatPrice")
    SchedulerSeatPrice getOneSchedulerSeatPrice(@RequestParam("schedulerId")Long schedulerId, @RequestParam("areaLevel")Long areaLevel);
}
