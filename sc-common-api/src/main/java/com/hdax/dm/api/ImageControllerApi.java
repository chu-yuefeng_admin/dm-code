package com.hdax.dm.api;

import com.hdax.dm.entity.base.Image;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface ImageControllerApi {
    //商品对应图片信息
    @RequestMapping(path = {"/rest/shareImage"})
    Image shareImage(@RequestParam("targetId")Long targetId,@RequestParam("type")Long type,@RequestParam("category")Long category);
   /* // 用户登录头像
    @RequestMapping(path = "/rest/loginImage")
    Image loginImage(@RequestParam("targetId")Long targetId);
    //首页公共获取商品图片接口 今日推荐的商品图片  即将预售
    @RequestMapping(path = "/rest/indexHomeImage")
    Image indexHomeImage(@RequestParam("targetId")Long targetId);*/
}
