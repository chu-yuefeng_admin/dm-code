package com.hdax.dm.api;

import com.hdax.dm.entity.scheduler.SchedulerSeat;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface SchedulerSeatControllerApi{
    /**
     * 通过排期id  和X轴 y轴获取 排期座位信息
     * @param schedulerId
     * @param X
     * @param Y
     * @return
     */
    @PostMapping(path = "scheduler/OneSchedulerSeat")
    SchedulerSeat getOneSchedulerSeat(@RequestParam("schedulerId")Long schedulerId,@RequestParam("X") Long X,@RequestParam("Y") Long Y);
    /**
     * 通过排期id  和X轴 y轴修改 排期座位信息
     * @return
     */
    @PostMapping(path = "scheduler/UpOneSchedulerSeat")
    int UpOneSchedulerSeat(@RequestBody SchedulerSeat schedulerSeat);
    /**
     * 通过订单号 修改 排期座位信息
     * @return
     */
    @PostMapping(path = "scheduler/UpTwoSchedulerSeat")
    boolean UpTwoSchedulerSeat(@RequestParam("orderNo")String orderNo);
    /**
     * 通过订单号 修改 排期座位状态为 3
     * @return
     */
    @PostMapping(path = "scheduler/UpThreeSchedulerSeat")
    boolean UpThreeSchedulerSeat(@RequestParam("orderNo")String orderNo);
}
