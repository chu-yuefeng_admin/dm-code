package com.hdax.dm.auth;

import cn.hutool.json.JSONUtil;
import com.hdax.dm.utils.ResponseUtil;
import io.jsonwebtoken.*;
import com.hdax.dm.token.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Component
public class AuthIntercepter implements HandlerInterceptor{
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /*private static TokenUtil tokenUtil;

    private static StringRedisTemplate stringRedisTemplate;

    @Autowired
    public void setTokenUtil(TokenUtil tokenUtil) {
        AuthIntercepter.tokenUtil = tokenUtil;
    }
    @Autowired
    public void setstringRedisTemplate(StringRedisTemplate stringRedisTemplate) {
        AuthIntercepter.stringRedisTemplate = stringRedisTemplate;
    }*/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("使用拦截器");
        //最常用的登录拦截、或是权限校验、或是防重复提交、或是根据业务像12306去校验购票时间,总之可以去做很多的事情。
        boolean flag = request.getRequestURI().contains("/p/"); //请求路径 不存在p   直接放行
        if(!flag) return true;
        String userId = "";
        String token1  =request.getHeader("token");
        Claims claims = null;
        try {
            claims = tokenUtil.token(token1);
            userId = claims.getId();
        }catch (NullPointerException e){
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println(JSONUtil.toJsonStr(ResponseUtil.returnFail("1305","账号异常，请重新登录")));
            return false;
        }
        //获取redis存储的token
        String token = stringRedisTemplate.opsForValue().get(userId);
        Claims claims1 = tokenUtil.token(token);
        String userId1  =  claims1.getId();
        if(userId1.equals(userId)) {
            return  true;
        }else{
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println(JSONUtil.toJsonStr(ResponseUtil.returnFail("1305","账号异常，请重新登录")));
            return false;
        }
        //先根据请求的token 去 redis  进行判断
        // 根据 token  解密  获取用户id
    }
}
