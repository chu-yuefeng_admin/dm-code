package com.hdax.dm.base.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.base.Image;

public interface ImageMapper extends BaseMapper<Image> {
}
