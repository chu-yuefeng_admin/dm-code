package com.hdax.dm.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.base.Image;

public interface ImageService extends IService<Image> {
}
