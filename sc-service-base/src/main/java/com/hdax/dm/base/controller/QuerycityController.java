package com.hdax.dm.base.controller;

import com.hdax.dm.base.dto.AreaDto;
import com.hdax.dm.base.dto.CarouselDto;
import com.hdax.dm.base.service.QuerycityService;
import com.hdax.dm.entity.base.Area;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/base")
//搜索的API市区控制层   用于商品搜索页查询市区信息
public class QuerycityController {
    @Autowired
    private QuerycityService querycityService;
    @PostMapping(path = "list/querycity")
    public CommonResponse<List<AreaDto>> querycity(){
        List<AreaDto> areaDtos =new ArrayList<>();
        querycityService.Querycitys().forEach(area -> {
            AreaDto areaDto = new AreaDto();
            BeanUtils.copyProperties(area,areaDto);
            areaDtos.add(areaDto);
        });
        return ResponseUtil.returnSuccess(areaDtos);
    }
}
