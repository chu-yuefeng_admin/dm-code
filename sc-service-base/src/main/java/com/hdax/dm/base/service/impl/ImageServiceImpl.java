package com.hdax.dm.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.base.mappers.ImageMapper;
import com.hdax.dm.base.service.ImageService;
import com.hdax.dm.entity.base.Image;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements ImageService {

}
