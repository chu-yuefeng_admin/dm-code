package com.hdax.dm.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.base.dto.CarouselDto;
import com.hdax.dm.base.feign.ItemFeign;
import com.hdax.dm.base.mappers.ImageMapper;
import com.hdax.dm.base.service.CarouselService;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CarouselServiceImpl extends ServiceImpl<ImageMapper, Image> implements CarouselService {
    //调用商品接口  获取商品信息
    @Autowired
    private ItemFeign itemFeign;
    @Override
    public List<CarouselDto> carousels(){
        //通过MP  分页查询第一页 显示4条
        IPage<Image> page = new Page<>(1,4);
        QueryWrapper<Image> queryWrapper = new QueryWrapper<>();
        //查询的条件 1.type(图片类型 1轮播图 2海报图) 2.category(图片分类 0用户头像 1商品图片) 3. sort 排序
        queryWrapper.eq("type",1L);
        queryWrapper.eq("category",1L);
        queryWrapper.orderByDesc("sort");
        //通过  MP  page 查询的结果进行分页 getRecords  将返回结果变成集合类型
        List<Image> images = this.page(page,queryWrapper).getRecords();
        //实例化 CarouselDto 需要向前端返回的整合类
        List<CarouselDto> carousels = new ArrayList<>();
        images.forEach(image -> {//遍历根据指定条件查询到的多个图片信息
            //通过远程调用 把指定的图片的信息  查询出来指定商品
            Item item = itemFeign.item(image.getTargetId());
            // 然后循环拼接到carousels 集合中返回
            carousels.add(CarouselDto.builder()
                    .id(image.getTargetId())
                    .imgUrl(image.getImgUrl())
                    .itemName(item.getItemName())
                    .minPrice(item.getMinPrice())
                    .build());
        });
        return carousels;
    }
}
