package com.hdax.dm.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.base.dto.CarouselDto;
import com.hdax.dm.entity.base.Image;

import java.util.List;
public interface CarouselService  extends IService<Image> {
    /**
     * 首页轮播查询
     * 查询规则： 按照时间降序查询最近的4个轮播图片
     *
     * @return
     */
    List<CarouselDto> carousels();
}
