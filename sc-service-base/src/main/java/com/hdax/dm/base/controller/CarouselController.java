package com.hdax.dm.base.controller;

import com.hdax.dm.base.dto.CarouselDto;
import com.hdax.dm.base.service.CarouselService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/base")
//轮播图控制层
public class CarouselController {

    @Autowired
    private CarouselService carouselService;

   @PostMapping(path = "/index/carousel")
    public CommonResponse<List<CarouselDto>> carousel(){
        List<CarouselDto> carousels = carouselService.carousels();
       for (CarouselDto carousel : carousels) {
           System.out.println(carousel);
       }
        return ResponseUtil.returnSuccess(carousels);
    }
}