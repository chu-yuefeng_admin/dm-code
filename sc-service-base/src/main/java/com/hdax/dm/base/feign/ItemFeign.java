package com.hdax.dm.base.feign;

import com.hdax.dm.api.ItemControllerApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

// 商品的远程调用 接口
@FeignClient(name = "dm-item")
@Component
public interface ItemFeign extends ItemControllerApi {
}
