package com.hdax.dm.base.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hdax.dm.api.ImageControllerApi;
import com.hdax.dm.base.service.ImageService;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 远程调用服务接口（图片服务提供者）
 */
@Service    //虽然继承了 但是这个service  必须加
 public class RestImageService implements ImageControllerApi {
    /**
     * 提供者提供的数据,继承了接口,注解都继承了,不用写
     * 这里以后就是提供者提供数据用相当于是提供者的controller,那个controller是真正的前端调用的时候才会用到的
     */
    @Autowired
    private ImageService imageService;
    @Override
    public Image shareImage(Long targetId, Long type, Long category) {
        QueryWrapper<Image> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type",type);
        queryWrapper.eq("category",category);
        queryWrapper.eq("targetId",targetId);
        return imageService.getOne(queryWrapper);
    }


   /* @Override
    //因为继承之后可以  把这个路径注释掉了
   *//* @PostMapping(path = "/rest/image")*//*
    public Image image(@RequestParam("targetId") Long targetId) {
        QueryWrapper<Image> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type",2L);
        queryWrapper.eq("category",1L);
        queryWrapper.eq("targetId",targetId);
        return imageService.getOne(queryWrapper);
    }*/
/*    @Override
    public Image loginImage(Long targetId) {
        QueryWrapper<Image> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type",0L);
        queryWrapper.eq("category",0L);
        queryWrapper.eq("targetId",targetId);
        return imageService.getOne(queryWrapper);
    }

    @Override
    public Image indexHomeImage(Long targetId) {
        QueryWrapper<Image> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type",0L);
        queryWrapper.eq("category",1L);
        queryWrapper.eq("targetId",targetId);
        return imageService.getOne(queryWrapper);
    }*/
}
