package com.hdax.dm.base.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.base.mappers.QuerycityMapper;
import com.hdax.dm.base.service.QuerycityService;
import com.hdax.dm.entity.base.Area;
import org.springframework.stereotype.Service;

import java.util.List;

//获取市区信息的业务类
@Service
public class QuerycityServiceImpl extends ServiceImpl<QuerycityMapper, Area> implements QuerycityService {

    @Override
    public List<Area> Querycitys(){
        return list();
    }
}
