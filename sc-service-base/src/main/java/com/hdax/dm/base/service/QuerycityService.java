package com.hdax.dm.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.base.Area;

import java.util.List;

public interface QuerycityService extends IService<Area> {
    //用于商品搜索列出的市区信息
   List<Area> Querycitys();
}
