package com.hdax.dm.common.exception;

import com.hdax.dm.common.code.CodeEnum;
import lombok.Getter;

public class DmException extends Exception{
    @Getter
    protected String errorCode;

    public DmException(String errorCode, String message){
        super(message);
        this.errorCode = errorCode;
    }

    public DmException(CodeEnum codeEnum){
        this(codeEnum.getErrorCode(), codeEnum.getMessage());
    }

    public DmException(String message){
        super(message);
    }
}
