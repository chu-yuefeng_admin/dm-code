package com.hdax.dm.common.code;

import lombok.Getter;

public enum CodeEnum {
    USER_VCODE_WITHOUT_TIMEOUT("1002","同一手机号60秒内只能发送一次验证码！"),
    USER_VCODE_NOT_EXISTS("1003","手机号已注册！"),
    USER_VCODE_NOT_ERROR("1008","验证码不正确，登录失败！"),
    USER_VCODE_PWD_ERROR("1006","账号或密码错误！"),
    USER_VCODE_CODE_LAPSE("1007","请重新获取验证码！"),
    USER_VCODE_IDCARD_ISNOT("1009","购票人已存在"),
    ORDER_VCODE_ERROR("2000","请求异常，请重试!");
    CodeEnum(String errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }

    @Getter
    private String errorCode;
    @Getter
    private String message;
}
