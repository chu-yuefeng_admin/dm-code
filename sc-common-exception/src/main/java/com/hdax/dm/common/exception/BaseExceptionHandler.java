package com.hdax.dm.common.exception;

import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice //增强通知
public class BaseExceptionHandler {
    @ExceptionHandler(value = {DmException.class})
    public CommonResponse exceptionHandel(DmException e){
        return ResponseUtil.returnFail(e.getErrorCode(),e.getMessage());
    }
}
