package com.hdax.dm.config.web;


import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.hdax.dm.auth.AuthIntercepter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Autowired
    private AuthIntercepter authIntercepter;
    public AuthIntercepter offerAuthIntercepter(){
        return authIntercepter;
    }
    /**
     * 过滤器
     */
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(offerAuthIntercepter())
                .addPathPatterns("/*/p/**") // api/order/xxx/xxx
                .order(0);
        super.addInterceptors(registry);
    }


    /**
     * 日期指定配置格式
     */
    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        System.out.println("sssssssssssssssssssssssssssssssssssssss");
        FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setCharset(Charset.forName("UTF-8"));
        fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
        fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
        fastJsonHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(
                MediaType.APPLICATION_JSON
        ));
        converters.add(fastJsonHttpMessageConverter);
    }
}
