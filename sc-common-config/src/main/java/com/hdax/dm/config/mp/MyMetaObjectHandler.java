package com.hdax.dm.config.mp;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Configuration
public class MyMetaObjectHandler implements MetaObjectHandler {
    //创建时间
    @Override
    public void insertFill(MetaObject metaObject) {
        System.out.println("ssssssssssssssssssssssss");
        this.strictInsertFill(metaObject, "createdTime", () -> LocalDateTime.now(), LocalDateTime.class); // 起始版本 3.3.3(推荐)
    }
    //修改后触发
    @Override
    public void updateFill(MetaObject metaObject) {
        System.out.println("ssssssssssssssssssssssss");
        this.strictUpdateFill(metaObject, "updatedTime", () -> LocalDateTime.now(), LocalDateTime.class); // 起始版本 3.3.3(推荐)
    }
}