package com.hdax.dm.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.common.code.CodeEnum;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.user.LinkUser;
import com.hdax.dm.user.dto.TicketBuyerDto;
import com.hdax.dm.user.mappers.LinkUserMapper;
import com.hdax.dm.user.service.TicketBuyerService;
import com.hdax.dm.user.vo.TickeBuyerVo;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketBuyerServiceImpl extends ServiceImpl<LinkUserMapper, LinkUser> implements TicketBuyerService {
    /**
     * 根据用户id 查询
     * @param userId
     * @return
     */
    @Override
    public List<TicketBuyerDto> ticketBuyerList(Integer userId) {
        QueryWrapper<LinkUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId",userId);
        List<TicketBuyerDto> ticketBuyerDtos = new ArrayList<>();
        list(queryWrapper).forEach(linkUser -> {
            TicketBuyerDto ticketBuyerDto = new TicketBuyerDto();
            BeanUtils.copyProperties(linkUser,ticketBuyerDto);
            ticketBuyerDto.setLinkId(linkUser.getId());
            ticketBuyerDtos.add(ticketBuyerDto);
        });
        return ticketBuyerDtos;
    }

    /**
     * 添加相关联系人信息
     * @param userId
     * @param tickeBuyerVo
     * @return
     */
    @Override
    public CommonResponse addTicketBuyer(Long userId,TickeBuyerVo tickeBuyerVo) {
        LinkUser linkUser = new LinkUser();
        BeanUtils.copyProperties(tickeBuyerVo,linkUser);
        linkUser.setUserId(userId);
        System.out.println(tickeBuyerVo);
        return ResponseUtil.returnSuccess(save(linkUser));
    }

    /**
     * 根据该用户id 和参数idCard 查询购票人是否存在
     * @param userId
     * @param idCard
     * @return
     * @throws DmException
     */
    @Override
    public CommonResponse validatebuyerexist(Long userId,String idCard) throws DmException {
        QueryWrapper<LinkUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId",userId);
        queryWrapper.eq("idCard",idCard);
        if(baseMapper.selectOne(queryWrapper)!=null){
            throw new DmException(CodeEnum.USER_VCODE_IDCARD_ISNOT);
        };
        return ResponseUtil.returnSuccess(null);
    }
    @Override
    public CommonResponse deleteticketbuyer(Long userId,Long linkId){
        QueryWrapper<LinkUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId",userId);
        queryWrapper.eq("id",linkId);
        baseMapper.delete(queryWrapper);
        return ResponseUtil.returnSuccess(null);
    }
}
