package com.hdax.dm.user.rest.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.api.LinkUserControllerApi;
import com.hdax.dm.entity.user.LinkUser;
import com.hdax.dm.user.mappers.LinkUserMapper;
import org.springframework.stereotype.Service;

@Service
public class RestLinkUser extends ServiceImpl<LinkUserMapper,LinkUser> implements LinkUserControllerApi {
    @Override
    public LinkUser getOneLinkUser(Long id, Long uId) {
        QueryWrapper<LinkUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",id);
        queryWrapper.eq("userId",uId);
        return this.getOne(queryWrapper);
    }
}
