package com.hdax.dm.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.common.code.CodeEnum;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.user.User;
import com.hdax.dm.token.TokenUtil;
import com.hdax.dm.user.dto.UserSmsDto;
import com.hdax.dm.user.feign.ImageFeign;
import com.hdax.dm.user.mappers.UserMapper;
import com.hdax.dm.user.service.UserLoginService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
@Service
public class UserLoginServiceImpl extends ServiceImpl<UserMapper, User> implements UserLoginService {
    //用于java操作redis中的String
    // 存储值  Map<String,String,List,Set,Map,ZSet>
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    //服务消息中间件
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    // 图片 接口
    private ImageFeign imageFeign;
    //认证自定义工具类
    @Autowired
    private TokenUtil tokenUtil;
    /**
     * 公共的产生随机数的方法
     * @return
     */
/*    public String verifyNumber(){

    }*/
    /**
     * 验证码验证
     * @param phone
     * @return
     * @throws DmException
     */
    @Override
    public CommonResponse msg(String phone,String servletPath) throws DmException,Exception{
        String flag ="";
        System.out.println(servletPath);
        if(servletPath.equals("/user/code")){//根据访问路径进行代码复用
            flag="_vcode";
        }else{
            System.out.println("/user/msg");
            flag="_vmsg";
        }
        String num = "0123456789";
        StringBuffer vcode = new StringBuffer();
        //String phone = params.get("phone");
        //获取前端的参数
        Object value = stringRedisTemplate.opsForValue().get(phone+flag);
        //1.有此手机号的信息，直接发送验证码登录
        if(value !=null){
            //判断是否已经再60秒内发过一次验证了
            throw new DmException(CodeEnum.USER_VCODE_WITHOUT_TIMEOUT);
        }
        for (int i = 1; i <= 4 ; i++) {
            //循环拼接四位随机数
            char n =  num.charAt((int)(Math.random()*num.length())); // num.charAt 截取0-9 的随机数(int)Math.random()*num.length()  生成0-9的数
            //这样循环四次  正好四位随机数
            vcode.append(n);
        }
        //第一个参数是键  第二个是值  三个 多长时间 失效   四个时间单位
        stringRedisTemplate.opsForValue().set(phone+flag,vcode.toString(),70, TimeUnit.SECONDS);
        Map<String,String > params = new HashMap<>();
        params.put("phone",phone);
        params.put("vcode",vcode.toString());
        rabbitTemplate.convertAndSend("sms",params);
        return ResponseUtil.returnSuccess(null);
    }

    /**
     * 短信验证码登录
     * @param phone 手机号
     * @param vcode 验证码s
     * @return  短信验证码登录  _vmsg
     * @throws DmException
     */
    @Override
    public CommonResponse msgLogin(String phone, String vcode) throws DmException {
        UserSmsDto userSmsDto = new UserSmsDto();//声明要向前端返回的对象
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        //1.通过此手机号查出来该账号信息
        userQueryWrapper.eq("phone",phone);
        //1.先判断验证码是否一致  在判断 是否存在该手机号
        String phone1 = phone+"_vmsg";
        //redis存储的真实有效的验证码
        String code = stringRedisTemplate.opsForValue().get(phone1);
        //验证码失败抛出指定异常
        if(code==null) {
            throw new DmException(CodeEnum.USER_VCODE_CODE_LAPSE);
        }
        //两者不一样则抛出异常
        if(!vcode.equals(code)||vcode=="") {
            throw new DmException(CodeEnum.USER_VCODE_NOT_ERROR);//代表和发送的验证码不一致
        }
        //需要 先从数据库查一下 是否有该手机号
        User dmUser = baseMapper.selectOne(userQueryWrapper);
        if(dmUser ==null){//等于空说明没有此手机号直接注册一个
            User createUser = new User();
            createUser.setPhone(phone);
            createUser.setNickName(phone+"_vcode");
            //2.没有此手机号的信息，通过此手机号注册之后  生成验证码进行登录
            int rows = baseMapper.insert(createUser);
        }
        //验证码一致  根据手机号获取 该账户的对应信息，返回对应信息
        //需要 先从数据库查一下 是否有该手机号
        User user = getOne(userQueryWrapper);//根据手机号查询到的结果
        BeanUtils.copyProperties(user, userSmsDto);// 属性相同的  会被拷贝进去  1参 向 2参拷贝
        Map<String,String> tokenParams = new HashMap<>();//将user 中的简单信息保存到 map中
        tokenParams.put("nickName",user.getNickName());
        tokenParams.put("phone",user.getPhone());
        tokenParams.put("realName",user.getRealName());
        tokenParams.put("id",user.getId().toString());
        //将 map传入token工具类参数
        userSmsDto.setToken(tokenUtil.createToken(tokenParams));
        userSmsDto.setExtTime(1000*60*60*2);
        //向redis 中存储 token 信息
        stringRedisTemplate.opsForValue().set(String.valueOf(user.getId()),tokenUtil.createToken(tokenParams),60*60*2, TimeUnit.SECONDS);
        return ResponseUtil.returnSuccess(userSmsDto);
    }

    /**
     *  手机号密码登录
     * @param phone
     * @param password
     * @return
     * @throws DmException
     */
    @Override
    public CommonResponse login(String phone,String password) throws DmException {
        //1.获取账号密码
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("phone",phone);
        //对输入的密码进行 加密操作
        userQueryWrapper.eq("password", DigestUtils.md5DigestAsHex(password.getBytes(StandardCharsets.UTF_8)));
        //根据手机号 密码 去数据库中查找 此用户信息
        User user = baseMapper.selectOne(userQueryWrapper);
        if(user == null){//进行判断 是否查找到
            //为空  说明没有 则抛出异常  下面的代码就不会执行了
            throw new DmException(CodeEnum.USER_VCODE_PWD_ERROR);
        }
        //获取到image信息  因为返回DTO中有不包含到用户头像的信息
        Image image = imageFeign.shareImage(user.getId(),0L,0L);
        //进行Token验证
        Map<String,String> tokenParams = new HashMap<>();//将user 中的简单信息保存到 map中
        tokenParams.put("nickName",user.getNickName());
        tokenParams.put("phone",user.getPhone());
        tokenParams.put("realName",user.getRealName());
        tokenParams.put("id",user.getId().toString());
        UserSmsDto userSmsDto =  UserSmsDto.builder()
                .token(tokenUtil.createToken(tokenParams))
                .extTime(1000*60*60*2)
                .imgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl())
                .imageId(image ==null?0:image.getId())
                .build();
        //把输入的密码转换成MDT 的方式
        //2.根据账号密码查询出来 该账号信息
        //3.对查询出来的信息 做判断是否 为空  为空 账号或者密码错误
        //向redis 中存储 token 信息
        stringRedisTemplate.opsForValue().set(String.valueOf(user.getId()),tokenUtil.createToken(tokenParams),60*60*2, TimeUnit.SECONDS);
        return ResponseUtil.returnSuccess(userSmsDto);
    }

    /**
     *  用户的注册
     * @param phone 手机号
     * @param vcode 验证码
     * @param password 密码_vcode 代表 是 注册用户的验证码
     * @return
     * @throws DmException
     */
    @Override
    public CommonResponse register(String phone, String vcode, String password) throws DmException {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("phone",phone);
        //需要 先从数据库查一下 是否有该手机号
        User dmUser = baseMapper.selectOne(userQueryWrapper);
        if(dmUser!=null){//等于空说明没有此手机号直接注册一个
            throw new DmException(CodeEnum.USER_VCODE_NOT_EXISTS);//代表该手机号已经注册过  直接抛异常
        }
        // 拼接 redis 中存储的 注册用的键
        String phone1 = phone+"_vcode";
        //两者不一样则抛出异常
        if(!stringRedisTemplate.opsForValue().get(phone1).equals(vcode)) {
            throw new DmException(CodeEnum.USER_VCODE_NOT_ERROR);//代表和发送的验证码不一致
        }
        //对密码 进行MD5加密
        String passwordY =  DigestUtils.md5DigestAsHex(password.getBytes(StandardCharsets.UTF_8));
        System.out.println(passwordY);
        User createUser = User.builder()
                .phone(phone)
                .password(passwordY)
                .realName(phone+"_vcode")
                .nickName(phone+"_vcode")
                .build();
        createUser.setPhone(phone);
        //2.没有此手机号的信息，通过此手机号注册之后  生成验证码进行登录
        int rows = baseMapper.insert(createUser);
        return ResponseUtil.returnSuccess(null);
    }
}
