package com.hdax.dm.user.dto;

import lombok.Data;

//用户信息
@Data
public class UserInfoDto {
    private String nikeName;
    private String phone;
    private String realName;
    private Long sex;
    private String idCard;
    private String birthday;
    private String hobby;
    private String imgUrl;
}
