package com.hdax.dm.user.controller;

import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.token.TokenUtil;
import com.hdax.dm.user.dto.TicketBuyerDto;
import com.hdax.dm.user.service.TicketBuyerService;
import com.hdax.dm.user.vo.TickeBuyerVo;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/user")
public class TicketBuyerListController {
    @Autowired
    private TicketBuyerService ticketBuyerService;
    //认证自定义工具类
    @Autowired
    private TokenUtil tokenUtil;
    /**
     * 通过该用户的id获取相关购票人的信息
     * @param
     * @return
     */
    @PostMapping(path = "p/userOrder/ticketbuyerlist")
    public CommonResponse<List<TicketBuyerDto>> getTicketBuyer(@RequestBody Map<String,Long> param, HttpServletRequest request){
        String token = request.getHeader("token");
        Claims claims = tokenUtil.token(token);
//        Integer userId = Integer.parseInt(claims.getId());
        return ResponseUtil.returnSuccess(ticketBuyerService.ticketBuyerList(Integer.parseInt(claims.getId())));
    }

    /**
     * 通过vo参数对象  进行添加联系人信息
     * @param tickeBuyerVo
     * @return
     */
    @PostMapping(path = "p/userOrder/addticketbuyer")
    public CommonResponse addTicketBuyer(@RequestBody TickeBuyerVo tickeBuyerVo,HttpServletRequest request){
        String token = request.getHeader("token");
        return ResponseUtil.returnSuccess(ticketBuyerService.addTicketBuyer(Long.parseLong(tokenUtil.token(token).getId()),tickeBuyerVo));
    }
    @PostMapping(path = "p/userOrder/validatebuyerexist")
    public CommonResponse validatebuyerexist(@RequestBody Map<String,String> param,HttpServletRequest request) throws DmException {
        String token = request.getHeader("token");
        return ResponseUtil.returnSuccess(ticketBuyerService.validatebuyerexist(Long.parseLong(tokenUtil.token(token).getId()),param.get("idCard")));
    }
    @PostMapping(path = "p/userOrder/deleteticketbuyer")
    public CommonResponse deleteticketbuyer(@RequestBody Map<String,String> param,HttpServletRequest request){
        String token = request.getHeader("token");
        return ResponseUtil.returnSuccess(ticketBuyerService.deleteticketbuyer
                (Long.parseLong(tokenUtil.token(token).getId()),Long.parseLong(param.get("linkId"))));
    }

}
