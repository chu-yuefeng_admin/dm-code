package com.hdax.dm.user.vo;

import lombok.Data;

/**
 * 前端传入的参   添加常用购票人
 */
@Data
public class TickeBuyerVo {
    private String  name;//用户姓名
    private String  idCard;//身份证号码
    private Long cardType;//卡类型(0:身份证,1:军官证)
}
