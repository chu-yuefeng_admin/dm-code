package com.hdax.dm.user.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.user.LinkUser;

public interface LinkUserMapper extends BaseMapper<LinkUser>{
}
