package com.hdax.dm.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.user.User;
import com.hdax.dm.user.dto.UserInfoDto;
import com.hdax.dm.user.mappers.UserMapper;
import com.hdax.dm.user.service.UserInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl extends ServiceImpl<UserMapper, User> implements UserInfoService {
    @Override
    public UserInfoDto UserInfo(Long uid) {
        UserInfoDto userInfoDto =new UserInfoDto();
        BeanUtils.copyProperties(getById(uid),userInfoDto);
        //远程调用获取用户头像
        //----
        return userInfoDto;
    }
}
