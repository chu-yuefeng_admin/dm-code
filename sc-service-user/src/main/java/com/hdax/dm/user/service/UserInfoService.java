package com.hdax.dm.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.user.User;
import com.hdax.dm.user.dto.UserInfoDto;

public interface UserInfoService extends IService<User> {
    /**
     * 根据用户id 获取指定信息
     * @param uid
     * @return
     */
    UserInfoDto UserInfo(Long uid);
}
