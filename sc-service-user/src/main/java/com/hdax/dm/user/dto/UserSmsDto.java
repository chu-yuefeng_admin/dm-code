package com.hdax.dm.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//用于验证码登录返回的data
public class UserSmsDto {
    private String token; //token标识
    private Integer extTime;//有效时间
    private long genTime;//当前时间
    private Long userId;//用户主键
    private String phone;//手机号
    private String realName;//真实用户姓名
    private String  nickName;//用户昵称
    private Long sex;//性别(0:男,1:女)
    private String idCard;//身份证号
    private String birthday;//
    private String hobby;//爱好
    private Long imageId;//头像ID
    private String imgUrl;//头像图片的URL
}
