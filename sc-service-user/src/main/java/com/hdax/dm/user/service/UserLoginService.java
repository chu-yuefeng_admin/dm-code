package com.hdax.dm.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.user.User;
import com.hdax.dm.utils.CommonResponse;


public interface UserLoginService extends IService<User> {
    //验证码验证 或者注册
    CommonResponse msg(String phone,String flag) throws DmException,Exception;
    //短信验证码登录
    CommonResponse msgLogin(String phone,String vcode) throws DmException;
    //账号密码登录
    CommonResponse login(String phone,String password) throws DmException ;
    //手机号注册
    CommonResponse register(String phone,String vcode,String password) throws DmException;
}
