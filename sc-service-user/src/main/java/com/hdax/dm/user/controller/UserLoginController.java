package com.hdax.dm.user.controller;

import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.user.service.UserLoginService;
import com.hdax.dm.utils.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(path = "/user")
public class UserLoginController {
    @Autowired
    private UserLoginService userLoginService;
    //登录短信验证码验证
    @PostMapping(value = { "code", "msg" })
    public CommonResponse msg(@RequestBody Map<String,String> params, HttpServletRequest request) throws DmException,Exception{
        String servletPath = request.getServletPath();//获取网名 也就是/vaule
        //TODO:  登录验证码  和注册验证码  访问同一方法问题
        //把获取到的手机号存储到session里面
        /*if (session.getAttribute(phone+"_vcode")!=null){
            System.out.println(session.getAttribute(phone+"_vcode"));
            //不等于空说明  有这个手机号  然后抛出异常信息
            throw new DmException(CodeEnum.USER_VCODE_WITHOUT_TIMEOUT);
        }*/
        /* session.setAttribute(phone+"_vcode",phone);*/
        //生成四位随机数
        return userLoginService.msg(params.get("phone"),servletPath);
    }
    //局部异常处理
    /*@ExceptionHandler(value = Exception.class)
    public void exceptionHandler(Exception e){
        if(e instanceof NullPointerException){

        }
     }*/

    /**
     * 短信验证码登录接口
     * @param params
     * @return
     */
    @PostMapping(path = "msg/login")
    public CommonResponse msgLogin(@RequestBody Map<String,String> params) throws DmException {
        //String phone = params.get("phone").toString()+"_vcode";
        String vcode = params.get("vcode").toString();
        //System.out.println(phone);
        System.out.println(vcode);
        return userLoginService.msgLogin(params.get("phone").toString(),vcode);
    }

    /**
     * 账号密码登录
     * @param params
     * @return
     * @throws DmException
     */
    @PostMapping(path = "login")
    public CommonResponse login(@RequestBody Map<String,String> params) throws DmException {
        return userLoginService.login(params.get("phone").toString(),params.get("password").toString());
    }
    //注册短信验证码验证
    /*@PostMapping(path = "code")
    public CommonResponse code(@RequestBody Map<String,String> params,) throws DmException {
        return null;
    }*/
    /**
     * 用户注册
     * @param params
     * @return
     * @throws DmException
     */
    @PostMapping(path = "register")
    public CommonResponse register(@RequestBody Map<String,String> params) throws DmException{
        return userLoginService.register(params.get("phone").toString(),params.get("vcode").toString(),params.get("password").toString());
    }
}
