package com.hdax.dm.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.user.LinkUser;
import com.hdax.dm.user.dto.TicketBuyerDto;
import com.hdax.dm.user.vo.TickeBuyerVo;
import com.hdax.dm.utils.CommonResponse;

import java.util.List;

public interface TicketBuyerService extends IService<LinkUser> {
    /**
     * 根据userid  获取相关联系人
     * @param userId
     * @return
     */
    List<TicketBuyerDto> ticketBuyerList(Integer userId);

    /**
     * 根据当前用户 id  添加一条购票人信息
     * @param userId
     * @param tickeBuyerVo
     * @return
     */
    CommonResponse addTicketBuyer(Long userId,TickeBuyerVo tickeBuyerVo);

    /**
     * 根据用户身份证号 和当前用户id 判断该联系人是否存在
     * @param userId
     * @param idCard
     * @return
     * @throws DmException
     */
    CommonResponse validatebuyerexist(Long userId,String idCard) throws DmException;

    /**
     * 根据 当前用户id 和联系人id 删除对应联系人信息
     * @param userId
     * @param linkId
     * @return
     */
    CommonResponse deleteticketbuyer(Long userId,Long linkId);
}
