package com.hdax.dm.user.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.user.User;
import com.hdax.dm.utils.CommonResponse;

//用户登录接口
public interface UserMapper extends BaseMapper<User> {
    //查询是否有该手机号
    CommonResponse selectPhoneYseNo(String phone);
}
