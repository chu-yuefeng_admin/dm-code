package com.hdax.dm.user.dto;

import lombok.Data;

//该用户的相关购票人
@Data
public class TicketBuyerDto {
    private Long linkId;//联系人ID
    private String name;//用户姓名
    private String idCard;//身份证号码
    private Long cardType;//字符串	卡类型(0:身份证,1:军官证)
}
