package com.hdax.dm.user.controller;

import com.hdax.dm.token.TokenUtil;
import com.hdax.dm.user.dto.UserInfoDto;
import com.hdax.dm.user.service.UserInfoService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(path = "/user")
public class UserInfoController {
    //认证自定义工具类
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private UserInfoService userInfoService;
    @PostMapping(path ="p/querypersoninfo")
    public CommonResponse<UserInfoDto> querypersoninfo(@RequestBody Map<String,Long> param, HttpServletRequest request){
        String token = request.getHeader("token");
        return ResponseUtil.returnSuccess(userInfoService.UserInfo(Long.parseLong(tokenUtil.token(token).getId())));
    }
}
