package com.hdax.dm.item.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Cinema;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.ItemRowsDto;
import com.hdax.dm.item.dto.ItemSearchResultDto;
import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.feign.ImageFeign;
import com.hdax.dm.item.mappers.CinemaMapper;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.mappers.ItemTypeMapper;
import com.hdax.dm.item.service.QuerygoodsinfosService;
import com.hdax.dm.item.vo.ItemSearchVo;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuerygoodsinfosServiceImpl extends ServiceImpl<ItemMapper,Item> implements QuerygoodsinfosService {
    //es 实现对数据的增删改查操作
    @Autowired
    private RestHighLevelClient client;
    @Autowired
    private ImageFeign imageFeign;
    @Autowired
    private CinemaMapper cinemaMapper;

    /**
     * es 数据库中快速检索商品信息
     * @param vo
     * @return
     * @throws Exception
     */
    @Override
    public ItemSearchResultDto itemSearch(ItemSearchVo vo) throws Exception {

        SearchRequest request = new SearchRequest("items"); //注入要搜索的索引id

        SearchSourceBuilder builder = new SearchSourceBuilder();

        HighlightBuilder highlightBuilder = new HighlightBuilder();

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        if(vo.getKeyword()!=null && !vo.getKeyword().trim().equals("")){

            boolQueryBuilder.must(QueryBuilders.matchQuery("itemname",vo.getKeyword()));

            boolQueryBuilder.should(QueryBuilders.matchQuery("abstractmessage",vo.getKeyword()));

            HighlightBuilder.Field highlightTitle = new HighlightBuilder.Field("itemname");//高亮字段是什么
            highlightTitle.highlighterType("unified");//内置高亮的语法
            highlightBuilder.field(highlightTitle);//设定高亮 语法
        }
        //城市条件拼接
        if(vo.getAreaId()!=0){
            boolQueryBuilder.must(QueryBuilders.termQuery("areaid",vo.getAreaId()));
        }
        //and
        //一二级分类
        if(vo.getItemTypeId1()!=0){
            boolQueryBuilder.must(QueryBuilders.termQuery("itemtype1id",vo.getItemTypeId1()));
        }
        if(vo.getItemTypeId2()!=0){
            boolQueryBuilder.must(QueryBuilders.termQuery("itemtype2id",vo.getItemTypeId2()));
        }
        //时间范围查询
        if(!vo.getStartTime().equals("")&& !vo.getEndTime().equals("")){
            RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("starttime");
            rangeQueryBuilder.gte(vo.getStartTime()+"T00:00:00.000Z");//>=时间
            rangeQueryBuilder.lte(vo.getEndTime()+"T00:00:00.000Z");//<=时间
            boolQueryBuilder.must(rangeQueryBuilder);
        }
        //时间 排序
        if (vo.getSort().equals("recommend")) builder.sort("isrecommend", SortOrder.DESC);
        if (vo.getSort().equals("recentShow")) builder.sort("starttime", SortOrder.DESC);
        if (vo.getSort().equals("reccentSell")) builder.sort("createtime", SortOrder.DESC);

        //6.执行查询封装
        builder.query(boolQueryBuilder);
        //设置高亮的样式
        highlightBuilder.requireFieldMatch(true);
        highlightBuilder.preTags("<span style='color:red;'>");//凡是根据我们关键字搜索出来的词条都会被这个包裹
        highlightBuilder.postTags("</span>");
        builder.highlighter(highlightBuilder);
        //8.分页查询
        builder.from((vo.getCurrentPage()-1)*vo.getPageSize());
        builder.size(vo.getPageSize());

        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);//发送请求，得到响应

        SearchHit[] searchHits = response.getHits().getHits();
        //10.初始化 前端Dto对象
        ItemSearchResultDto resultDto = new ItemSearchResultDto();
        //设置当前页数
        resultDto.setCurrentPage(vo.getCurrentPage());
        //设置每页条数
        resultDto.setPageSize(vo.getPageSize());
        //初始化 rows 集合
        resultDto.setRows(new ArrayList<>());
        //设置总共多少页
        resultDto.setPageCount(response.getHits().getTotalHits().value % vo.getPageSize() == 0?
                response.getHits().getTotalHits().value / vo.getPageSize():
                response.getHits().getTotalHits().value / vo.getPageSize()+1);
        //设置 显示的总条数
        resultDto.setTotal(response.getHits().getTotalHits().value);

        for (int i = 0; i < searchHits.length; i++) {
            //每一行搜索到的商品数据
            ItemRowsDto rowsDto = JSON.parseObject(searchHits[i].getSourceAsString(), ItemRowsDto.class);
            //远程调用 给每个商品查询图片
            Image image =imageFeign.shareImage(rowsDto.getId(),2L,1L);
            rowsDto.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl());
            //判断 keyword 关键字 是否为空
            if (vo.getKeyword()!=null&&!vo.getKeyword().equals("")){
                rowsDto.setItemName(StringUtils.arrayToDelimitedString(
                        searchHits[i].getHighlightFields().get("itemname").getFragments(),""));
            }
            resultDto.getRows().add(rowsDto);
        }
        return resultDto;
    }

    /**
     * 搜索猜你喜欢
     * @param itemTypeId  分类id
     * @return
     */
    @Override
    public List<ItemsDto> Guesslikes(Long itemTypeId) {
        List<ItemsDto> itemsDtos =new ArrayList<>();
        baseMapper.Guesslikes(itemTypeId).forEach(itemsDto -> {
            //远程调用
            Image image =imageFeign.shareImage(itemsDto.getId(),2L,1L);
            itemsDto.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl());
            itemsDtos.add(itemsDto);
        });
        return itemsDtos;
    }

    /**
     * 根据  节目 适合的 年龄 进行 查询
     * @param ageGroup
     * @param limit
     * @return
     */
    @Override
    public List<ItemsDto> getParentChild(Long ageGroup, Long limit) {
        return this.publicMethod(ageGroup,limit,"ageGroup");
    }

    /**
     * 根据商品类型id 按照指定条数进行返回
     * @param itemTypeId
     * @param limit
     * @return
     */
    @Override
    public List<ItemsDto> getGuessYouLike(Long itemTypeId, Long limit) {
        return this.publicMethod(itemTypeId,limit,"itemTypeId");
    }

    @Override
    public List<ItemsDto> publicMethod(Long number, Long limit, String flag) {
        QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
        if(flag.equals("ageGroup")){
            queryWrapper.eq("ageGroup",number);
        }else{
            queryWrapper.eq("itemType1Id",number);
        }
        List<ItemsDto> itemsDtos = new ArrayList<>();
        IPage<Item> itemIPage = page(new Page<Item>(0,limit),queryWrapper);
        itemIPage.getRecords().forEach(item -> {
            ItemsDto itemsDto = new ItemsDto();
            BeanUtils.copyProperties(item,itemsDto);
            //调用dm_cinema 表 获取商品城市 和城市名称
            Cinema cinema =cinemaMapper.selectById(item.getCinemaId());
            itemsDto.setAreaId(cinema.getAreaId());//城市id
            itemsDto.setAreaName(cinema.getAreaName());//城市名称
            itemsDto.setAdress(cinema.getAddress());//节目所在地址
            //远程调用
            Image image =imageFeign.shareImage(itemsDto.getId(),2L,1L);
            itemsDto.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl());
            itemsDto.setStartDate(String.valueOf(item.getStartTime()));
            itemsDto.setEndDate(item.getEndTime());
            itemsDtos.add(itemsDto);
        });
        return itemsDtos;
    }

}
