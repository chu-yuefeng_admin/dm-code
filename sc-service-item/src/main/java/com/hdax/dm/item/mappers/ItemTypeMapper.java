package com.hdax.dm.item.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.dto.SeniorityDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

// 表示商品的类型接口
public interface ItemTypeMapper extends BaseMapper<ItemType> {

}
