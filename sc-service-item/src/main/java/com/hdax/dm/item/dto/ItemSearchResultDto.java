package com.hdax.dm.item.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
//搜索 Dto
public class ItemSearchResultDto {
    private Integer currentPage;
    private Long pageCount;
    private Integer pageSize;
    private List<ItemRowsDto> rows = new ArrayList<>();
    private Long total;
}
