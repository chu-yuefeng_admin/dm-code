package com.hdax.dm.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.SortgoodsDto;
import com.hdax.dm.item.mappers.ItemTypeMapper;
import com.hdax.dm.item.service.SortgoodsService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class SortgoodsServiceImpl extends ServiceImpl<ItemTypeMapper,ItemType> implements SortgoodsService {
    @Override
    public List<SortgoodsDto> Sortgoods(Integer parent) {
        QueryWrapper<ItemType> itemTypeQueryWrapper = new QueryWrapper<>();
        itemTypeQueryWrapper.eq("parent",parent);
        List<SortgoodsDto> sortgoodsDtos = new ArrayList<>();
        list(itemTypeQueryWrapper).forEach(itemType -> {
            SortgoodsDto sortgoodsDto = new SortgoodsDto();
            BeanUtils.copyProperties(itemType,sortgoodsDto);
            sortgoodsDtos.add(sortgoodsDto);
        });
        return sortgoodsDtos;
    }
}
