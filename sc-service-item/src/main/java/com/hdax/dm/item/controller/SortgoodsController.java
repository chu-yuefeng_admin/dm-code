package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.IndexNavDto;
import com.hdax.dm.item.dto.SortgoodsDto;
import com.hdax.dm.item.service.IndexNavService;
import com.hdax.dm.item.service.SortgoodsService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

// 列表页 2. 查询分类接口
@RestController
@RequestMapping(path = "/item")
public class SortgoodsController {
    @Autowired
    private SortgoodsService sortgoodsService;
    @PostMapping(path = {"/list/sortgoods"})
    public CommonResponse<List<SortgoodsDto>> sortgoods(@RequestBody Map<String,Integer> params){
        return ResponseUtil.returnSuccess(sortgoodsService.Sortgoods(params.get("parent")));
    }
}
