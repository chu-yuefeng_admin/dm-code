package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//首页的公共Dto -轮播-等
public class IndexHomeDto {
    private Long id;//分类主键
    private String itemName;//节目名称
    private Double minPrice;//最低价格
    private String imgUrl;//热门节目宣传照片
}
