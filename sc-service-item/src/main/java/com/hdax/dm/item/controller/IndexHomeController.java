package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.IndexHomeDto;
import com.hdax.dm.item.service.RecommendService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

//首页公共控制类 首页今日推荐 即将预售
@RestController
@RequestMapping(path = "/item")
public class IndexHomeController {
    @Autowired
    private RecommendService recommendService;
    //首页今日推荐 //首页即将预售
    @PostMapping(path = {"/index/recommend","/index/sell"})
    public CommonResponse<List<IndexHomeDto>> Recommend(HttpServletRequest request){
        String servletPath = request.getServletPath();//获取网名 也就是/vaule
        System.out.println(servletPath);
        return ResponseUtil.returnSuccess(recommendService.RecommendsOrSell(servletPath));
    }
    @PostMapping(path = {"/multiple/getCarouselData"})
    public CommonResponse<List<IndexHomeDto>> lineNav(@RequestBody Map<String,Long> param,HttpServletRequest request){
        String servletPath = request.getServletPath();//获取网名 也就是/vaule
        System.out.println(servletPath);
        return ResponseUtil.returnSuccess(recommendService.lineNav(param.get("itemTypeId"),servletPath));
    }
}
