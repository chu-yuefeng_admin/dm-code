package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.service.QuerygoodsinfosService;
import com.hdax.dm.item.vo.ItemSearchVo;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

//搜索页面
@RestController
@RequestMapping(path = "/item")
public class QuerygoodsinfosController {
    //根据城市、一级分类、二级分、时间、排序字段查询商品信息
    @Autowired
    private QuerygoodsinfosService querygoodsinfosService;
    @PostMapping(path = "list/querygoodsinfos")
    public CommonResponse ItemSearch(@RequestBody ItemSearchVo vo)  throws Exception{
        return ResponseUtil.returnSuccess(querygoodsinfosService.itemSearch(vo));
    }

    /**
     * 搜索页面的猜你喜欢控制层
     * @return
     */
    @PostMapping("list/guesslike")
    public CommonResponse<List<ItemsDto>> Guesslike(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(querygoodsinfosService.Guesslikes(params.get("itemTypeId")));
    }
    @PostMapping("multiple/getParentChild")
    public CommonResponse<List<ItemsDto>> getParentChild(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(querygoodsinfosService.getParentChild(params.get("ageGroup"),params.get("limit")));
    }
    @PostMapping("multiple/getGuessYouLike")
    public CommonResponse<List<ItemsDto>> getGuessYouLike(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(querygoodsinfosService.getGuessYouLike(params.get("itemTypeId"),params.get("limit")));
    }
    @PostMapping("multiple/getWonderfulData")
    public CommonResponse<List<ItemsDto>> getWonderfulData(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(querygoodsinfosService.getGuessYouLike(params.get("itemTypeId"),params.get("limit")));
    }
    @PostMapping("multiple/getPopularSearch")
    public CommonResponse<List<ItemsDto>> getPopularSearch(@RequestBody Map<String,Long> params){
        return ResponseUtil.returnSuccess(querygoodsinfosService.getGuessYouLike(params.get("itemTypeId"),params.get("limit")));
    }
}
