package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.ItemSearchResultDto;
import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.vo.ItemSearchVo;

import java.util.List;

public interface QuerygoodsinfosService extends IService<Item> {
    //搜索页面的多条件搜索商品信息
    ItemSearchResultDto itemSearch(ItemSearchVo vo) throws Exception;
    //搜索页 猜你喜欢
    List<ItemsDto> Guesslikes(Long itemTypeId);

    /**
     * 根据年龄段 显示条数 查询指定商品信息
     * @param ageGroup
     * @param limit
     * @return
     */
    List<ItemsDto> getParentChild(Long ageGroup,Long limit);

    /**
     * 根据分类id 显示条数  查询指定商品信息
     * @param itemTypeId
     * @param limit
     * @return
     */
    List<ItemsDto> getGuessYouLike(Long itemTypeId, Long limit);

    /**
     * 上面两个接口公共方
     * @param number
     * @param limit
     * @param flag
     * @return
     */
    List<ItemsDto> publicMethod(Long number,Long limit,String flag);
}
