package com.hdax.dm.item.vo;

import lombok.Data;

@Data
public class CalendarDataVo {
    private Long month;//月份(1,2,3,4.....)
    private Long year;//年份(2018)
    private Long itemTypeId;//商品分类主键
}
