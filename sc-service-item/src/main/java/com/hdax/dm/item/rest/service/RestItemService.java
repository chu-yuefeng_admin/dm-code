package com.hdax.dm.item.rest.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.api.ItemControllerApi;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.mappers.ItemMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class RestItemService extends ServiceImpl<ItemMapper, Item> implements ItemControllerApi {

    @Override
    @PostMapping(path = "/rest/item")
    public Item item(@RequestParam("id") Long id) {
        return baseMapper.selectById(id);
    }
}

