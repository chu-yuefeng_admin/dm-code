package com.hdax.dm.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.FloorDto;
import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.feign.ImageFeign;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.mappers.ItemTypeMapper;
import com.hdax.dm.item.service.FloorService;
import com.hdax.dm.item.service.IndexNavService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class FloorServiceImpl extends ServiceImpl<ItemMapper, Item> implements FloorService {
    @Autowired
    private IndexNavService indexNavService;
    @Autowired
    private ImageFeign imageFeign;
    @Autowired
    private ItemMapper itemMapper;
    @Override
    public List<FloorDto> Floors() {
        List<FloorDto> floorDtos = new ArrayList<>();
        Integer[] itemetype = {1, 2, 3, 4, 5};//楼层 也就是商品的分类
        for (int i = 0; i < itemetype.length; i++) {
            ItemType itemType = indexNavService.itemType(itemetype[i]);
            FloorDto floorDto = FloorDto.builder()
                    .index(itemetype[i])
                    .itemTypeId(itemetype[i])
                    .itemTypeName(itemType.getItemType())
                    .build();
                List<ItemsDto> items = new ArrayList<>();
                itemMapper.ItemsDtos(itemetype[i]).forEach(items1 -> {
                    ItemsDto items2 = new ItemsDto();
                    Image image = imageFeign.shareImage(items1.getId(),2L,1L);
                    BeanUtils.copyProperties(items1,items2);
                    items2.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl()); //设置商品图片
                    items.add(items2);
                });
                /*QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
                itemQueryWrapper.eq("itemType1Id", itemetype[i1]);
                List<Items> items = new ArrayList<>();
                IPage<Item> itemTypeIPage = page(new Page<Item>(0, 5), itemQueryWrapper);
                itemTypeIPage.getRecords().forEach(item -> {
                    Items items1 = new Items();
                    BeanUtils.copyProperties(item, items1);
                    Image image = imageFeign.indexHomeImage(item.getId());
                    //因为  有的图片可能没有信息  点操作  可能会报 空指针异常  所以还要进行 三元运算符判断
                    items1.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl()); //设置商品图片
                    items1.setAreaName("s");
                    items1.setAreaId(1);
                    items1.setAddress("sssssssss");
                    items.add(items1);
                });
                */
                floorDto.setItems(items);
            floorDtos.add(floorDto);
        }
        return floorDtos;
    }
}
