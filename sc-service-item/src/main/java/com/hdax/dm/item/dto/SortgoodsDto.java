package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// 整合 商品分类信息 类
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SortgoodsDto {
        private Long id;
        private String itemType;
        private Long level;
        private Long parent;
        private String aliasName;
}
