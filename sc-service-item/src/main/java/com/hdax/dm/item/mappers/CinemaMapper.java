package com.hdax.dm.item.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.item.Cinema;

public interface CinemaMapper extends BaseMapper<Cinema> {
}
