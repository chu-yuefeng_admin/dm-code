package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// 获取首页横向导航数据的Dot
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//首页导航Dto
public class IndexLineNavDto {
    private Long id;
    private String itemType;//分类名称
    private Long level;//分类级别
    private String aliasName;//分类别名
    private Long key;//模板标识
}
