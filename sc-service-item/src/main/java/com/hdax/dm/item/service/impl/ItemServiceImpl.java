package com.hdax.dm.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.common.code.CodeEnum;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.item.Cinema;
import com.hdax.dm.entity.item.ItemComment;
import com.hdax.dm.entity.order.Order;
import com.hdax.dm.item.dto.CommentsDto;
import com.hdax.dm.item.dto.ItemDescDto;
import com.hdax.dm.item.dto.ItemDto;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.feign.ImageFeign;
import com.hdax.dm.item.feign.OrderFeign;
import com.hdax.dm.item.mappers.CinemaMapper;
import com.hdax.dm.item.mappers.ItemCommentMapper;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.service.ItemService;
import com.hdax.dm.item.vo.ItemCommentVo;
import com.hdax.dm.token.TokenUtil;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
//商品详情页
public class ItemServiceImpl extends ServiceImpl<ItemMapper,Item> implements ItemService {

    @Autowired
    private ImageFeign imageFeign;
    @Autowired
    private CinemaMapper cinemaMapper;
    @Autowired
    private ItemCommentMapper itemCommentMapper;
    @Override
    public ItemDto item(Long id) {
        Item item = baseMapper.selectById(id);
        Image image = imageFeign.shareImage(item.getId(),2L,1L);
        return ItemDto.builder()
                .itemName(item.getItemName())
                .imgUrl(image.getImgUrl())
                .build();
    }
    /**
     * 商品详情信息业务逻辑
     * @param itemId
     * @return
     */
    @Override
    public ItemDescDto getItems(Long itemId) {
        QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
        itemQueryWrapper.eq("id",itemId);
        Item item = this.getOne(itemQueryWrapper);
        Cinema cinema = cinemaMapper.selectById(item.getCinemaId());
        ItemDescDto itemDescDto = new ItemDescDto();
        BeanUtils.copyProperties(item,itemDescDto);
        //远程调用 给每个商品查询图片
        Image image =imageFeign.shareImage(item.getId(),2L,0L);
        itemDescDto.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl());
        itemDescDto.setAdress(cinema.getAddress());
        itemDescDto.setAreaId(cinema.getAreaId());
        itemDescDto.setAreaName(cinema.getAreaName());
        return itemDescDto;
    }

    /**
     * 根据商品id  查询出该商品的评价
     * @param itemId
     * @return
     */
    @Override
    public List<CommentsDto> getComments(Long itemId){
        List<CommentsDto> commentsDtos = new ArrayList<>();
        QueryWrapper<ItemComment> itemCommentQueryWrapper = new QueryWrapper<>();
        itemCommentQueryWrapper.eq("itemid",itemId);
        itemCommentMapper.selectList(itemCommentQueryWrapper).forEach(itemComment -> {
            CommentsDto commentsDto = new CommentsDto();
            BeanUtils.copyProperties(itemComment,commentsDto);
            //远程图片调用


            commentsDtos.add(commentsDto);
        });
        return commentsDtos;
    }
}
