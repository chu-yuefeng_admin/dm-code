package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.CalendarDataDto;
import com.hdax.dm.item.service.CalendarDataService;
import com.hdax.dm.item.service.QuerygoodsinfosService;
import com.hdax.dm.item.vo.CalendarDataVo;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//搜索页面
@RestController
@RequestMapping(path = "/item")
public class getCalendarDataController {
    @Autowired
    private CalendarDataService calendarDataService;
    @PostMapping(path = "multiple/getCalendarData")
    public CommonResponse<List<CalendarDataDto>> getCarouselData(@RequestBody CalendarDataVo calendarDataVo){
        return ResponseUtil.returnSuccess(calendarDataService.getCarouselData(0L,0L,calendarDataVo.getItemTypeId()));
    }
}
