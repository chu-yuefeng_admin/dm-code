package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.FloorDto;

import java.util.List;

//首页楼层商品
public interface FloorService extends IService<Item> {
    //查询首页楼层接口
    List<FloorDto> Floors();
}
