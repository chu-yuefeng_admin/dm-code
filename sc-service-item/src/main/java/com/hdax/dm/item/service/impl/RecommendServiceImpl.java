package com.hdax.dm.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.IndexHomeDto;
import com.hdax.dm.item.feign.ImageFeign;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecommendServiceImpl extends ServiceImpl<ItemMapper,Item> implements RecommendService {
    @Autowired
    private ImageFeign imageFeign;

    /**
     * 今日推荐 和 即将开售
     * @return
     */
    @Override
    public List<IndexHomeDto> RecommendsOrSell(String Head) {
        Long state = 2L;
        if(Head.equals("/item/index/recommend"))state = 3L;
        //1.根据条件查询出来即将开售的商品
        QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
        //状态(1:项目待定2:预售/预订 3:开始售票4:演出开始)
        itemQueryWrapper.eq("state",state);//开始售票
        itemQueryWrapper.eq("isBanner",0L);//是否热销 默认0
        itemQueryWrapper.eq("isRecommend",1L);//是否推荐 推荐1
        IPage<Item> itemTypeIPage = page(new Page<>(0,5),itemQueryWrapper);
        List<IndexHomeDto> recommendDtos = new ArrayList<>();
        itemTypeIPage.getRecords().forEach(item -> {//遍历
            //1.远程调用 没个今日推荐的 商品图
            Image image = imageFeign.shareImage(item.getId(),2L,2L);
            //2.对recommendDtos 集合dot 进行拼接
            IndexHomeDto recommendDto = IndexHomeDto.builder()
                    .id(item.getId())//返回的是商品id
                    .itemName(item.getItemName())
                    .minPrice(item.getMinPrice())
                    .imgUrl(image == null || image.getImgUrl() == null ?"":image.getImgUrl())
                    .build();
            recommendDtos.add(recommendDto);
        });
        return recommendDtos;
    }

    @Override
    public List<IndexHomeDto> lineNav(Long itemTypeId,String Head) {
        Long type = 1L;
        if(Head.equals("/item/multiple/getCarouselData"))type = 2L;
        //1.根据条件查询出来即将开售的商品
        QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
        //状态(1:项目待定2:预售/预订 3:开始售票4:演出开始)
        itemQueryWrapper.eq("state",3L);//开始售票
        itemQueryWrapper.eq("isBanner",1L);//是否热销 默认0
        itemQueryWrapper.eq("isRecommend",1L);//是否推荐 推荐1
        itemQueryWrapper.eq("itemType1Id",itemTypeId);
        IPage<Item> itemTypeIPage = page(new Page<>(0,5),itemQueryWrapper);
        List<IndexHomeDto> recommendDtos = new ArrayList<>();
        Long finalType = type;
        itemTypeIPage.getRecords().forEach(item -> {//遍历
            System.out.println(item.getId());
            System.out.println(finalType);
            //1.远程调用 每个 商品图
            Image image = imageFeign.shareImage(item.getId(),2L, finalType);
            //2.对recommendDtos 集合dot 进行拼接
            IndexHomeDto recommendDto = IndexHomeDto.builder()
                    .id(item.getId())//返回的是商品id
                    .itemName(item.getItemName())
                    .minPrice(item.getMinPrice())
                    .imgUrl(image == null || image.getImgUrl() == null ?"":image.getImgUrl())
                    .build();
            recommendDtos.add(recommendDto);
        });
        return recommendDtos;
    }
}
