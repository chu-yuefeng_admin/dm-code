package com.hdax.dm.item.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.item.ItemComment;
import com.hdax.dm.item.mappers.ItemCommentMapper;
import com.hdax.dm.item.vo.ItemCommentVo;

public interface ItemCommentService extends IService<ItemComment> {
    boolean addComments(ItemCommentVo itemCommentVo, String token) throws DmException;
}
