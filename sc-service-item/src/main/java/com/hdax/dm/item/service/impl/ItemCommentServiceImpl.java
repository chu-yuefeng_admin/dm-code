package com.hdax.dm.item.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.item.ItemComment;
import com.hdax.dm.item.feign.OrderFeign;
import com.hdax.dm.item.mappers.ItemCommentMapper;
import com.hdax.dm.item.service.ItemCommentService;
import com.hdax.dm.item.vo.ItemCommentVo;
import com.hdax.dm.token.TokenUtil;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemCommentServiceImpl extends ServiceImpl<ItemCommentMapper, ItemComment>implements ItemCommentService{
    @Autowired
    private OrderFeign orderFeign;
    @Autowired
    private TokenUtil tokenUtil;
    @Override
    public boolean addComments(ItemCommentVo itemCommentVo, String token) throws DmException {
        Long userId = Long.parseLong(tokenUtil.token(token).getId());
        //添加评论信息
        ItemComment itemComment = new ItemComment();
        BeanUtils.copyProperties(itemCommentVo,itemComment);
        //查询此用户是否买过该商品
        if (orderFeign.getCount(userId,itemComment.getItemId())<=0){
            //用户没用买过此商品无法进行评论
            throw new DmException("6666","请购该商品再进行评论");
        }
        itemComment.setUserId(userId);
        itemComment.setContent(itemCommentVo.getComment());
        return this.save(itemComment);
    }
}
