package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.item.dto.SeniorityDto;

import java.util.List;

public interface SeniorityService{
    List<SeniorityDto> Seniority(Integer itemTypeId);
}
