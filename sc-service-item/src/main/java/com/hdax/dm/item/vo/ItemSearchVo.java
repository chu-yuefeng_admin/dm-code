package com.hdax.dm.item.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//用于商品列表页查询商品参数
public class ItemSearchVo {
    private String keyword;//搜索关键词
    private Long areaId;//城市Id区域id
    private Long itemTypeId1;//一级分类
    private Long itemTypeId2;//二级分类
    private String startTime;//演出开始时间
    private String endTime;//演出结束时间
    private String sort;//商品排序方式
    private Integer currentPage=1;//当前页
    private Integer pageSize=16;//每页条数
}
