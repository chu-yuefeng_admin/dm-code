package com.hdax.dm.item.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.dto.SeniorityDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ItemMapper extends BaseMapper<Item> {
    /**
     * 根据id  查询指定商品条数
     * @param id
     * @return
     */
    List<ItemsDto> ItemsDtos(@Param("id") Integer id);
    /**
     * 根据分类id 查询指定商品字段
     * @param itemTypeId
     * @return
     */
    List<SeniorityDto> Senioritys(@Param("itemTypeId") Integer itemTypeId);
    /**
     * 根据分类id 查询 指定商品信息
     * @param itemTypeId
     * @return
     */
    List<ItemsDto> Guesslikes(@Param("itemTypeId") Long itemTypeId);
    @Select(
            "SELECT *  FROM `dm_item` WHERE itemtype1id=#{itemtypeId} AND MONTH(createdTime) = #{month} LIMIT 0,31"
    )
    List<Item> getCarouselData(@Param("itemtypeId")Long itemtypeId,@Param("month") Long month);
}
