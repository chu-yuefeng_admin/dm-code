package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SeniorityDto {
    private Long id;//分类id
    private String itemName;//节目名称
    private Integer areaId;//城市id
    private String areaName;//城市名称
}
