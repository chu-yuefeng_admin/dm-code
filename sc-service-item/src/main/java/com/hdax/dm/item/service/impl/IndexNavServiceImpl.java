package com.hdax.dm.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.IndexHomeDto;
import com.hdax.dm.item.dto.IndexLineNavDto;
import com.hdax.dm.item.dto.IndexNavDto;
import com.hdax.dm.item.feign.ImageFeign;
import com.hdax.dm.item.mappers.ItemTypeMapper;
import com.hdax.dm.item.service.IndexNavService;
import com.hdax.dm.item.service.ItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class IndexNavServiceImpl extends ServiceImpl<ItemTypeMapper, ItemType> implements IndexNavService {
    @Autowired
    //商品接口
    private ItemService itemService;
    @Autowired
    // 图片 接口
    private ImageFeign imageFeign;
    @Override
    //实现  商品分类信息
    public List<IndexNavDto> nav() {
        //MP 提供的条件查询商品分类信息 一级分类条件
        QueryWrapper<ItemType> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("level",1L);//  分类级别;select;(1:1级,2:2级)
        wrapper1.eq("parent",0L);// 父级类型  本身就是父
        //查询一级分类
        List<ItemType> firstTypes = list(wrapper1);//根据条件查询出来的商品分类 给集合赋值
        List<IndexNavDto> dtos = new ArrayList<>();// 初始化 适合前端的分类类型整合
        //////////////////////////////// 分类信息  firstTypes
        firstTypes.forEach(itemType1 -> {//遍历 这个查询出来的 分类信息
            //二级分类的条件
            QueryWrapper<ItemType> queryWrapper2  = new QueryWrapper<>();
            queryWrapper2 .eq("level",2L);
            queryWrapper2.eq("parent",itemType1.getId());
            //查询二级分类
            List<ItemType> secondTypes = list(queryWrapper2);//secondTypes英文翻译  第二种分类
            List<IndexNavDto> children = new ArrayList<>();//子级
            secondTypes.forEach(itemType2 -> children.add(IndexNavDto.builder()
                    //拼接适合前端的分类信息
                    .id(itemType2.getId())//类型id
                    .aliasName(itemType2.getAliasName())//类型别名
                    .level(itemType2.getLevel())//类型级别
                    .parent(itemType2.getParent())//父级类型
                    .itemType(itemType2.getItemType())//类别名称
                    .build()));
            //查询热门商品
            QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
            //加入条件
            itemQueryWrapper.eq("isBanner",1L);//是否推荐(0:默认 1:热推)
            itemQueryWrapper.eq("isRecommend",1L);//是否推荐(0:不推荐 1:推荐)
            itemQueryWrapper.eq("itemType1Id",itemType1.getId());//节目类型管理ID //一级分类的商品
            //itemQueryWrapper.orderByDesc("startTime");//开始演出时间   排序降序  最新在前
            List<Item> items = itemService.page(new Page<Item>(1,5),itemQueryWrapper).getRecords();// 根据条件查询出来的商品信息 集合进行赋值
            List<IndexHomeDto> hotItemDtos = new ArrayList<>(); //存储热门商品信息// 初始化  适合前端的  商品类型信息

            items.forEach(item-> {//对 商品信息进行遍历
                IndexHomeDto hotItem = new IndexHomeDto();//初始化 适合前端的 商品信息
                BeanUtils.copyProperties(item,hotItem);// 属性相同的  会被拷贝进去  1参 向 2参拷贝
                //获取每个商品的图片
                //因为每一个商品对应一个图片所以需要  需要根据商品id  查询出来对应的图片信息
                Image image =imageFeign.shareImage(item.getId(),2L,1L);
                //因为  有的图片可能没有信息  点操作  可能会报 空指针异常  所以还要进行 三元运算符判断
                hotItem.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl()); //设置商品图片
                hotItemDtos.add(hotItem); //将每一个商品对象 add到 集合中
            });
            dtos.add(IndexNavDto.builder()
                    .id(itemType1.getId())
                    .aliasName(itemType1.getAliasName())
                    .level(itemType1.getLevel())
                    .parent(itemType1.getParent())
                    .children(children)
                    .hotItems(hotItemDtos)
                    .itemType(itemType1.getItemType())
                    .build());
        });
        return dtos;
    }
    @Override
    public List<IndexLineNavDto> indexLineNav() {// 首页分类导航
        QueryWrapper itemTypeQueryWrapper = new QueryWrapper<>();
        //使用 MP的分页插件 对分类进行 分页处理
        IPage<ItemType> itemTypeIPage = page(new Page<>(0,8),itemTypeQueryWrapper);
        List<IndexLineNavDto> lineNavDtos =new ArrayList<>();
        itemTypeIPage.getRecords().forEach(itemType -> {
            IndexLineNavDto lineNavDto = new IndexLineNavDto();
            BeanUtils.copyProperties(itemType,lineNavDto);
            lineNavDtos.add(lineNavDto);
        });
        return lineNavDtos;
    }
    @Override
    public ItemType itemType(Integer id) {
        return baseMapper.selectById(id);
    }
}
