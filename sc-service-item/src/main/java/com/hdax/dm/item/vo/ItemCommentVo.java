package com.hdax.dm.item.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemCommentVo {

    private Long itemId; //用户编号
    private Long userId; //用户编号
    private Long score; //评分
    private String comment;//评论内容
}

