package com.hdax.dm.item.dto;

import lombok.Data;

//剧评Dto
@Data
public class CommentsDto {
    private Long itemId;
    private Long userId;
    private String imgUrl;
    private Long score;
    private String content;
    private String createdTime;
}
