package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//商品详情Dto
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemDescDto {
    private Long id;
    private String abstractMessage;//商品简介
    private String itemName;//节目名称
    private Long itemType1Id;//一级分类ID
    private String itemType1Name;//一级分类名称
    private Long itemType2Id;//二级分类ID
    private String itemType2Name;//二级分类名称
    private Long areaId;//区域Id
    private String areaName;//区域名称
    private LocalDateTime startTime;//演出开始时间
    private LocalDateTime endTime;//演出结束时间
    private Long state;//状态(1:项目待定2:预售/预订3:开始售票4:演出开始)
    private String basicDescription;//基础描述
    private String projectDescription;//项目描述
    private String reminderDescription;//温馨提示
    private String imgUrl;//图片URL
    private Long cinemaId;//剧场Id
    private String adress;//剧场地址
    private Double longitude;//剧场经度
    private Double latitude;//剧场纬度
    private Double avgScore;//平均评分
    private Long commentCount;//评论人数  22
}
