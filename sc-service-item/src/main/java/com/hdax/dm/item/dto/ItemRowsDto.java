package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//搜索 商品Dto
public class ItemRowsDto {
    private Long Id;   //商品排期id
    private String imgUrl; //商品图片url
    private String areaName; //城市名称
    private String itemName; //节目名称
    private String abstractMessage; //摘要
    private String startTime; //开始时间
    private String endTime; //结束时间
    private Double minPrice; //最低价格
    private Double maxPrice; //最高价格
    private String address; //剧场地址
    private String latitude; //剧场经度
    private String longitude; //剧场纬度
}
