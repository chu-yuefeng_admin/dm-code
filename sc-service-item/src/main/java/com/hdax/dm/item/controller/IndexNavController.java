package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.IndexLineNavDto;
import com.hdax.dm.item.dto.IndexNavDto;
import com.hdax.dm.item.service.IndexNavService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 首页导航
 * 一级，二级，热门商品
 */
@RestController
@RequestMapping(path = "/item")
public class IndexNavController {
    @Autowired
    private IndexNavService indexNavService;
    //商品分类信息
    @PostMapping(path = {"/index/nav"})
    public CommonResponse<List<IndexNavDto>> nav(){
        return ResponseUtil.returnSuccess(indexNavService.nav());
    }
    // 获取首页横向导航
    @PostMapping(path = "/index/lineNav")
    public CommonResponse<List<IndexLineNavDto>> lineNav(){
        return ResponseUtil.returnSuccess(indexNavService.indexLineNav());
    }
}
