package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.IndexLineNavDto;
import com.hdax.dm.item.dto.IndexNavDto;
import com.hdax.dm.item.mappers.ItemTypeMapper;

import java.util.List;

//商品分类 接口
public interface IndexNavService extends IService<ItemType> {
    //整合  查询 商品分类信息
    List<IndexNavDto> nav();
    //整合  获取首页横向导航信息
    List<IndexLineNavDto> indexLineNav();
    //首页 楼层 1-5 分类信息
    ItemType itemType(Integer id);
}
