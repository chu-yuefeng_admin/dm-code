package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.SeniorityDto;
import com.hdax.dm.item.dto.SortgoodsDto;
import com.hdax.dm.item.service.SeniorityService;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

//首页不同分类排行
@RestController
@RequestMapping(path = "/item")
public class SeniorityController {
    @Autowired
    private  SeniorityService seniorityService;
    @PostMapping(path = "index/seniority")
    public CommonResponse<List<SeniorityDto>> Seniority(@RequestBody Map<String,Integer> params){
        return ResponseUtil.returnSuccess(seniorityService.Seniority(params.get("itemTypeId")));
    }
}
