package com.hdax.dm.item.feign;

import com.hdax.dm.api.OrderControllerApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(name="dm-order")
@Component
public interface OrderFeign extends OrderControllerApi {
}
