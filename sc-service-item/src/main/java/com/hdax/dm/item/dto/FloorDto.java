package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//楼层Dto
public class FloorDto {
   private int index; //楼层(1:1L 2:2L ...)
   private String  itemTypeName;//楼层名称
   private Integer itemTypeId;//剧目分类ID
   private List<ItemsDto> items;//排名前5的节目
}
