package com.hdax.dm.item.service.impl;

import com.hdax.dm.item.dto.SeniorityDto;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.mappers.ItemTypeMapper;
import com.hdax.dm.item.service.SeniorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SeniorityServiceImpl implements SeniorityService {
    @Autowired
    private ItemMapper itemMapper;
    @Override
    public List<SeniorityDto> Seniority(Integer itemTypeId) {
        return itemMapper.Senioritys(itemTypeId);
    }
}
