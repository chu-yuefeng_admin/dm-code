package com.hdax.dm.item.controller;

import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.item.service.ItemCommentService;
import com.hdax.dm.item.vo.ItemCommentVo;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/item")
public class ItemCommentsController {
   @Autowired
   private ItemCommentService itemCommentService;
   @PostMapping("p/desc/submitComments")
   public CommonResponse addComments(@RequestBody ItemCommentVo itemCommentVo, HttpServletRequest request) throws DmException {
       String token = request.getHeader("token");
       itemCommentService.addComments(itemCommentVo,token);
       return  ResponseUtil.returnSuccess(null);
   }
}
