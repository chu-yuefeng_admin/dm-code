package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.CalendarDataDto;

import java.util.List;

public interface CalendarDataService extends IService<Item> {
    List<CalendarDataDto> getCarouselData(Long month, Long year, Long itemTypeId);
}
