package com.hdax.dm.item.dto;

import lombok.Data;

import java.util.List;
@Data
public class CalendarDataDto {
    private Integer day;
    private List<ItemsDto> itemList;
}
