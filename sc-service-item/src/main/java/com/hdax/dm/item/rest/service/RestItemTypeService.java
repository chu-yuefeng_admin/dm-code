package com.hdax.dm.item.rest.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.api.ItemTypeControllerApi;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.mappers.ItemTypeMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@Service
public class RestItemTypeService extends ServiceImpl<ItemTypeMapper, ItemType> implements ItemTypeControllerApi {
    @Override
    public ItemType itemType(Integer id) {
        return baseMapper.selectById(id);
    }
}
