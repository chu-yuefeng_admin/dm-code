package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.FloorDto;
import com.hdax.dm.item.service.FloorService;
import com.hdax.dm.item.service.impl.FloorServiceImpl;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//首页楼层商品
@RestController
@RequestMapping(path = "/item")
public class FloorController {
    @Autowired
    private FloorService floorService;
    @PostMapping(path = "index/floor")
    public CommonResponse<List<FloorDto>> Floors(){
      return  ResponseUtil.returnSuccess(floorService.Floors());
    }
}
