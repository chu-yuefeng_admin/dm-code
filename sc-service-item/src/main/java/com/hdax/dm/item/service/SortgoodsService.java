package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.item.ItemType;
import com.hdax.dm.item.dto.SortgoodsDto;

import java.util.List;

public interface SortgoodsService extends IService<ItemType> {
    //整合  查询 商品分类信息
    List<SortgoodsDto> Sortgoods(Integer parent);
}
