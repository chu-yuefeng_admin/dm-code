package com.hdax.dm.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.base.Image;
import com.hdax.dm.entity.item.Cinema;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.CalendarDataDto;
import com.hdax.dm.item.dto.ItemDto;
import com.hdax.dm.item.dto.ItemsDto;
import com.hdax.dm.item.feign.ImageFeign;
import com.hdax.dm.item.mappers.CinemaMapper;
import com.hdax.dm.item.mappers.ItemMapper;
import com.hdax.dm.item.service.CalendarDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class CalendarDataServiceImpl  extends ServiceImpl<ItemMapper, Item> implements CalendarDataService {
    @Autowired
    private  ItemMapper itemMapper;
    @Autowired
    private ImageFeign imageFeign;
    @Autowired
    private CinemaMapper cinemaMapper;
    @Override
    public List<CalendarDataDto> getCarouselData(Long month, Long year, Long itemTypeId) {
        List<CalendarDataDto> calendarDataDtos = new ArrayList<>();
        List<Item> items = itemMapper.getCarouselData(itemTypeId,6L);
        for (int i =0;i<=30;i++) {
            CalendarDataDto calendarDataDto = new CalendarDataDto();
            List<ItemsDto> itemsDtos = new ArrayList<>();
            ItemsDto itemsDto = new ItemsDto();
            BeanUtils.copyProperties(items.get(i),itemsDto);
            //调用dm_cinema 表 获取商品城市 和城市名称
            Cinema cinema =cinemaMapper.selectById(items.get(i).getCinemaId());
            itemsDto.setAreaId(cinema.getAreaId());//城市id
            itemsDto.setAreaName(cinema.getAreaName());//城市名称
            itemsDto.setAdress(cinema.getAddress());//节目所在地址
            //远程调用
            Image image =imageFeign.shareImage(itemsDto.getId(),2L,1L);
            itemsDto.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl());
            itemsDto.setStartDate("2022年9月"+(i+1)+"日 11:56");
            itemsDto.setEndDate(items.get(i).getEndTime());
            itemsDtos.add(itemsDto);
            calendarDataDto.setDay(20220901+i);
            calendarDataDto.setItemList(itemsDtos);
            calendarDataDtos.add(calendarDataDto);
        }

        /*List<Item> items = itemMapper.getCarouselData(itemTypeId,6L);
        for (Item item : items) {
            count+=1;
            System.out.println(item.getId());
            ItemsDto itemsDto = new ItemsDto();
            BeanUtils.copyProperties(item,itemsDto);
            //调用dm_cinema 表 获取商品城市 和城市名称
            Cinema cinema =cinemaMapper.selectById(item.getCinemaId());
            itemsDto.setAreaId(cinema.getAreaId());//城市id
            itemsDto.setAreaName(cinema.getAreaName());//城市名称
            itemsDto.setAdress(cinema.getAddress());//节目所在地址
            //远程调用
            Image image =imageFeign.shareImage(itemsDto.getId(),2L,1L);
            itemsDto.setImgUrl(image == null || image.getImgUrl() == null ? "" : image.getImgUrl());
            itemsDto.setStartDate("2022年9月"+count+"日 11:56");
            itemsDto.setEndDate(item.getEndTime());
            itemsDtos.add(itemsDto);
        }
        calendarDataDto.setDay(20220905L);
        calendarDataDto.setItemsDtos(itemsDtos);
        calendarDataDtos.add(calendarDataDto);*/
        return calendarDataDtos;
    }
}
