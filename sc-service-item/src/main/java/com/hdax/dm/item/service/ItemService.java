package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.item.dto.CommentsDto;
import com.hdax.dm.item.dto.ItemDescDto;
import com.hdax.dm.item.dto.ItemDto;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.vo.ItemCommentVo;
import com.hdax.dm.utils.CommonResponse;

import java.util.List;

public interface ItemService extends IService<Item> {

    ItemDto item(Long id);
    //根据商品id 获取指定信息
    ItemDescDto getItems(Long itemId);
    //根据商品id 获取 剧评信息
    List<CommentsDto> getComments(Long itemId);
}
