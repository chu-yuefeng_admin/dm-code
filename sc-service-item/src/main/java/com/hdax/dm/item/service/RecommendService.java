package com.hdax.dm.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.item.dto.IndexHomeDto;

import java.util.List;

//即将开售  和 今日推荐
public interface RecommendService extends IService<Item> {
    //首页今日推荐 //即将开售 商品 信息
    // 代码复用 今日推荐和 即将开售写到了一个方法中
    List<IndexHomeDto> RecommendsOrSell(String HeadUrl);
    //根据分类id 获取指定信息   公共的 方法 需要根据路径Head判断是否是查询轮播商品还是普通的
    List<IndexHomeDto> lineNav(Long itemTypeId,String Head);
}
