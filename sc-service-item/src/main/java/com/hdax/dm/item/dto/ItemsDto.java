package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemsDto {
    private Long id;//分类id
    private String itemName;//节目名称
    private Long areaId;//城市
    private String areaName;//城市名称
    private String adress;//节目所在地址  address
    private String startDate;//节目开始日期
    private LocalDateTime endDate;//节目结束日期
    private String imgUrl;//节目宣传图片
    private Double minPrice;//最低价格
}
