package com.hdax.dm.item.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
// 整合 商品分类信息 类
//首页 菜单栏Dto
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndexNavDto {
    private Long id;
    private String itemType;
    private Long level;
    private Long parent;
    private String aliasName;
    private List<IndexNavDto> children;
    private List<IndexHomeDto> hotItems;
}
