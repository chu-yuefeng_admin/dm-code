package com.hdax.dm.item.controller;

import com.hdax.dm.item.dto.CommentsDto;
import com.hdax.dm.item.dto.ItemDescDto;
import com.hdax.dm.item.service.ItemService;
import com.hdax.dm.item.service.impl.ItemServiceImpl;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

//商品详情页的控制层
@RestController
@RequestMapping(path = "/item")
public class ItemDescController {
    @Autowired
    private ItemService itemService;
    @PostMapping(path = "desc/getItems")
    public CommonResponse<ItemDescDto> getItems(@RequestBody Map<String,Long> params){
        return  ResponseUtil.returnSuccess(itemService.getItems(params.get("id")));
    }
    @PostMapping(path = "desc/getComments")
    public CommonResponse<List<CommentsDto>> getComments(@RequestBody Map<String,Long> params){
        return  ResponseUtil.returnSuccess(itemService.getComments(params.get("id")));
    }
}
