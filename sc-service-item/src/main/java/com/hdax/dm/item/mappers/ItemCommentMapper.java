package com.hdax.dm.item.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.item.ItemComment;

//剧评
public interface ItemCommentMapper extends BaseMapper<ItemComment> {
}
