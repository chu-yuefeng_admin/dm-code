package com.hdax.dm.order.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.order.Order;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Param;

import java.util.List;

//订单接口
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 根据订单编号 和支付订单号   进行填充
     * @param orderNo
     * @param aliTradeNo
     * @return
     */
    @Update(
            "update dm_order SET orderType =2,aliTradeNo = #{aliTradeNo} where orderNo = #{orderNo} "
    )
    int UpStatePay(@Param("orderNo") String orderNo,@Param("aliTradeNo") String aliTradeNo);

    /**
     * 根据订单号查询 订单信息
     * @param orderNo
     * @return
     */
    @Select(
            "select * from dm_order where orderNo = #{orderNo}"
    )
    Order selectOne(@Param("orderNo") String orderNo);
    /**
     * 通过订单号  修改订单状态为-1   订单超时方法
     * @return
     */
    @Update(
            "update dm_order SET orderType =-1 where orderNo = #{orderNo} "
    )
    int OrderTimedOutState(@Param("orderNo")String orderNo);
    @Select({
            "<script>"+
                    "select * from dm_order " +
                    "<where>" +
                    "<if test=\" orderType!=null and  orderType!='' \" >"+
                    "orderType = #{orderType}"+
                    "</if>"+
                    "<if test='createTime!=0'>" +
                    "<if test='createTime==1'>" +
                    " and YEAR(createdTime) = YEAR(NOW())"+
                    "</if>"+
                    "<if test='createTime==2'>" +
                    " and createdTime between date_sub(now(),interval 6 month) and now()"+
                    "</if>"+
                    "</if>"+
                    "<if test=\" keyWord!=null and  keyWord!='' \" >"+
                    " and itemName like concat('%',#{keyWord},'%')"+
                    "</if>"+
                    " and userId = #{userId}"+
                    "</where>"+
                    " order by createdTime desc"+
                    "</script>"
    })
    List<Order> likeOrder(@Param("orderType") String orderType, @Param("createTime") String orderTime, @Param("keyWord")String keyWord, @Param("userId")Long userId);
}
