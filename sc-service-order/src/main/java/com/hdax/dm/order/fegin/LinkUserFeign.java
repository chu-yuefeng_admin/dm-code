package com.hdax.dm.order.fegin;

import com.hdax.dm.api.LinkUserControllerApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(name = "dm-user")
@Component
public interface LinkUserFeign extends LinkUserControllerApi {
}
