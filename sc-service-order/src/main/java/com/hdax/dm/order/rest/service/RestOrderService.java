package com.hdax.dm.order.rest.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.api.OrderControllerApi;
import com.hdax.dm.entity.order.Order;
import com.hdax.dm.order.mappers.OrderMapper;
import org.springframework.stereotype.Service;

@Service
public class RestOrderService extends ServiceImpl<OrderMapper,Order> implements OrderControllerApi {
    //根据排期id获取orderd的购买数量
    @Override
    public Long SelectOrder(Long scheduleId) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.and(q->{q.ne("status",1).eq("schedulerId",scheduleId);});
        System.out.println(baseMapper.selectCount(queryWrapper));
        return baseMapper.selectCount(queryWrapper);
    }
    //根据订单编号获取订单信息
    @Override
    public Order detail(String orderNo) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orderNo",orderNo);
        return getOne(queryWrapper);
    }

    @Override
    public boolean upOrderStateSuccess(String orderNo,String aliTradeNo) {
        boolean flag = false;
        if(baseMapper.UpStatePay(orderNo,aliTradeNo)>0)flag = true;
        return flag;
    }

    @Override
    public Long getCount(Long userId, Long itemId) {
            QueryWrapper<Order> queryWrapper = new QueryWrapper<Order>();
            queryWrapper.eq("userId",userId);
            queryWrapper.eq("itemId",itemId);
            return this.count(queryWrapper);
        }
}
