package com.hdax.dm.order.mq;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hdax.dm.entity.order.Order;
import com.hdax.dm.order.fegin.LinkUserFeign;
import com.hdax.dm.order.fegin.SchedulerSeatFeign;
import com.hdax.dm.order.fegin.SchedulerSeatPriceFeign;
import com.hdax.dm.order.mappers.OrderLinkUserMapper;
import com.hdax.dm.order.mappers.OrderMapper;
import com.hdax.dm.order.service.OrderLinkUserService;
import com.hdax.dm.order.service.SubmitOrderService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;


@RabbitListener(queues = {"linkuser_rollback","seat_rollback","order_rollback"})
@Component
public class OrderMqListener {
    @Autowired
    private OrderLinkUserService orderLinkUserService;
    @Autowired
    private SchedulerSeatFeign schedulerSeatFeign;
    @Autowired
    private SubmitOrderService submitOrderService;

    //订单联系人新增失败的数据回滚
    @RabbitHandler
    public void rollbackLinkUser(Channel channel, Message message, String orderNo) throws IOException {
        String queue =message.getMessageProperties().getConsumerQueue();//消息队列名
        long tag = message.getMessageProperties().getDeliveryTag();
        if(queue.equals("linkuser_rollback")){
            boolean flag = orderLinkUserService.DelOrderLink(orderNo);
            if(flag){
                //当上面删除方法执行成功   则手动确认  告诉我们服务器要删除这条消息
                channel.basicAck(tag,false);
            }else{
                //不为真的话  重试操作 让rabbitmq 重发这条消息   2参 是否批量执行消息   3参是否重回队列
                channel.basicNack(tag,false,false);
                //加quarzt 定时任务发送三次
                //依然无法消费，日志记录，通过人工干预进行处理    分布式事务 无论用哪一种事务都只能降低错误
            }
        }else if(queue.equals("seat_rollback")){
            System.out.println("zxzczxvcxvxcsdfefas");
            boolean flag1 = orderLinkUserService.DelOrderLink(orderNo);
            boolean flag2 =schedulerSeatFeign.UpTwoSchedulerSeat(orderNo);
            if(flag1&&flag2){
                channel.basicAck(tag,false);
            }else{
                //不为真的话  重试操作 让rabbitmq 重发这条消息   2参 是否批量执行消息   3参是否重回队列
                channel.basicNack(tag,false,false);
                //加quarzt 定时任务发送三次
                //依然无法消费  ，日志记录，通过人工干预进行处理    分布式事务 无论用哪一种事务都只能降低错误
            }
        }else if(queue.equals("order_rollback")){
            boolean flag1 = orderLinkUserService.DelOrderLink(orderNo);
            boolean flag2 = schedulerSeatFeign.UpTwoSchedulerSeat(orderNo);
            boolean flag3 = submitOrderService.DelOrder(orderNo);
            if(flag1&&flag2&&flag3){
                System.out.println(flag1);
                System.out.println(flag2);
                System.out.println(flag3);
                channel.basicAck(tag,false);
            }else{
                //不为真的话  重试操作 让rabbitmq 重发这条消息   2参 是否批量执行消息   3参是否重回队列
                channel.basicNack(tag,false,false);
                //加quarzt 定时任务发送三次
                //依然无法消费  ，日志记录，通过人工干预进行处理    分布式事务 无论用哪一种事务都只能降低错误
            }
        }
    }

}
