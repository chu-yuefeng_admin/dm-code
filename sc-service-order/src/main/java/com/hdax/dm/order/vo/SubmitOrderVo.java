package com.hdax.dm.order.vo;

import lombok.Data;

@Data
public class SubmitOrderVo {
    private String userId;//用户id
    private Long schedulerId;//排期Id
    private Long itemId;//商品Id
    private Long cinemaId;//剧场Id
    private Long sourceType;//订单来源(0:WEB端 1:手机端 2:其他客户端)
    private Long isNeedInvoice;//是否需要发票（0：不需要 1：需要）
    private Long invoiceType;//发票类型（0：个人 1：公司）
    private String invoiceHead;//发票抬头
    private String invoiceNo;//发票号
    private Long isNeedInsurance;//是否需要保险(0：不需要 1:需要)
    private String seatPositions;//座位坐标使用(x1_y1,x2_y2,...的形式提交)
    private String linkIds;//下单中选择的联系人(7,8的形式提交)

/*    //后加的
    private Long ticketNum;//购票数量
    private Double paymount;//购票价格*/
}
