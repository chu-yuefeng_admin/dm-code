package com.hdax.dm.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.entity.order.OrderLinkUser;
import com.hdax.dm.order.mappers.OrderLinkUserMapper;
import com.hdax.dm.order.service.OrderLinkUserService;
import org.springframework.stereotype.Service;

@Service
public class OrderLinkUserServiceImpl extends ServiceImpl<OrderLinkUserMapper, OrderLinkUser> implements OrderLinkUserService {
    /**
     * 根据订单编号删除 订单联系人表信息
     * @param OrderNo
     * @return
     */
    @Override
    public boolean DelOrderLink(String OrderNo) {
        UpdateWrapper<OrderLinkUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("orderId",OrderNo);
        return this.remove(updateWrapper);
    }
}
