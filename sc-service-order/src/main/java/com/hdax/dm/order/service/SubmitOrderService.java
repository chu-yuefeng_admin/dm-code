package com.hdax.dm.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.order.Order;
import com.hdax.dm.order.dto.ConfirmPayDto;
import com.hdax.dm.order.dto.OrdersDto;
import com.hdax.dm.order.vo.QueryorderlistVo;
import com.hdax.dm.order.vo.SubmitOrderVo;

import java.util.List;
import java.util.Map;

public interface SubmitOrderService extends IService<Order> {

    String SubimtOrder(Integer uId, SubmitOrderVo submitOrderVo) throws DmException;

    /**
     * 根据订单编号 和当前用户 获取 订单信息
     * @param orderNo
     * @return
     */
    ConfirmPayDto getOrderPay(Long orderNo,Long uId);

    /**
     * 根据 订单编号删除 订单信息
     * @param OrderNo
     * @return
     */
    boolean DelOrder(String OrderNo);

    /**
     * 根据用户id  获取多个订单信息
     * @param userId
     * @return
     */
    List<OrdersDto> queryorderlist(Map<String,String> param, Long userId);
}
