package com.hdax.dm.order.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdax.dm.entity.order.OrderLinkUser;

/**
 * 购票人接口
 */
public interface OrderLinkUserMapper extends BaseMapper<OrderLinkUser> {
}
