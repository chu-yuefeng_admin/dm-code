package com.hdax.dm.order.dto;

import lombok.Data;

/**
 * 订单信息Dto
 */
@Data
public class ConfirmPayDto {
    private String  orderNo;//订单编号
    private String  itemName;//商品名称
    //private String  seatNames;//逗号相隔
    //private Long    seatCount;//座位数量
    private Double payAmount;//应付金额
    private Double totalAmount;//订单金额
}
