package com.hdax.dm.order.fegin;

import com.hdax.dm.api.ItemControllerApi;
import com.hdax.dm.entity.item.Item;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * 远程商品信息
 */
@FeignClient(name="dm-item")
@Component
public interface ItemFeign extends ItemControllerApi {
}
