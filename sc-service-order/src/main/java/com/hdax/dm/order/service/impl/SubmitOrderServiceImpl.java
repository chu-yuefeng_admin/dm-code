package com.hdax.dm.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdax.dm.common.code.CodeEnum;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.entity.item.Item;
import com.hdax.dm.entity.order.Order;
import com.hdax.dm.entity.order.OrderLinkUser;
import com.hdax.dm.entity.scheduler.SchedulerSeat;
import com.hdax.dm.entity.scheduler.SchedulerSeatPrice;
import com.hdax.dm.entity.user.LinkUser;
import com.hdax.dm.order.dto.ConfirmPayDto;
import com.hdax.dm.order.dto.OrdersDto;
import com.hdax.dm.order.fegin.ItemFeign;
import com.hdax.dm.order.fegin.LinkUserFeign;
import com.hdax.dm.order.fegin.SchedulerSeatFeign;
import com.hdax.dm.order.fegin.SchedulerSeatPriceFeign;
import com.hdax.dm.order.listener.RabbitConfig;
import com.hdax.dm.order.mappers.OrderMapper;
import com.hdax.dm.order.service.OrderLinkUserService;
import com.hdax.dm.order.service.SubmitOrderService;
import com.hdax.dm.order.vo.QueryorderlistVo;
import com.hdax.dm.order.vo.SubmitOrderVo;
import com.hdax.dm.utils.OrderUtils;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class SubmitOrderServiceImpl extends ServiceImpl<OrderMapper,Order> implements SubmitOrderService {
    //商品远程调用接口
    @Autowired
    private ItemFeign itemFeign;
    @Autowired
    private SchedulerSeatFeign schedulerSeatFeign;
    @Autowired
    private SchedulerSeatPriceFeign schedulerSeatPriceFeign;
    @Autowired
    private OrderLinkUserService orderLinkUserService;
    @Autowired
    private LinkUserFeign linkUserFegin;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private RedisTemplate redisTemplate;
    //订单延时操作
    @Autowired
    private RabbitConfig myRabbitListener;

    /**
     * @param uId
     * @param submitOrderVo
     * @return 返回订单号
     */
    @Override
    public String SubimtOrder(Integer uId, SubmitOrderVo submitOrderVo) throws DmException {
        //要把 X-Y,X-Y  分割成  x   y   x   y  单个类型
        //1.先分割成 X-Y
        String[] XYs = submitOrderVo.getSeatPositions().split(",");
        for (int i = 0; i < XYs.length; i++) {
            String[] XY = XYs[i].split("_");
            Long X = Long.parseLong(XY[0]);//单个X
            Long Y = Long.parseLong(XY[1]);//单个Y
            SchedulerSeat schedulerSeat = schedulerSeatFeign.getOneSchedulerSeat(submitOrderVo.getSchedulerId(), X, Y);
            //判断用户是否同时进入到了下单页面 然后对该剧场id  和  x  和  y  座位进行判断该座位装填是否是  2 和  3
            if (schedulerSeat.getStatus() == 2 || schedulerSeat.getStatus() == 3) {
                throw new DmException("4444", "该座位已被锁定,请重新选择");
            }
        }
        //定义方法开始时间
        long startTime = System.currentTimeMillis();
        //进行分布式锁  确保数据的原子性
        boolean flag = redisTemplate.opsForValue().setIfAbsent("lock", "lock", 5000, TimeUnit.MILLISECONDS);
        //如果 返回为false  说明该锁被占用  要让其他进来的 等待
        if (!flag) {
            try {
                //线程0延迟执行
                Thread.sleep(3000);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
        /*while (flag){

        }*/
        //TODO 1.进行分布式事务 用来 防止 数据容错 一个方法执行了  而第二个方法  报错了  rabbitmqTemplate  进行方法消费
        // 实现思路
        //TODO 2.事务锁锁定剧场 用RedisTemplate 中的 sendnx 进行方法锁
        //订单价格
        Double orderPrice = 0d;
        //TODO 测试 添加的order信息
        //远程调用查询商品名称 1.根据submitOrderVo.getItemId();查询商品信息，
        Item item = itemFeign.item(submitOrderVo.getItemId());
        //使用工具类生成全网唯一订单号
        String orderNo = OrderUtils.getOrderCode(uId);
        Order order = new Order();
        //前端参数 和 实例订单类 进行字段拷贝
        BeanUtils.copyProperties(submitOrderVo, order);
        order.setItemId(item.getId());//商品id
        order.setItemName(item.getItemName());//商品名称
        order.setOrderNo(orderNo);//订单编号
        order.setOrderType(0L);
        Double yesInsurance = 0d;
        //对linkIds进行分割
        String[] linkIds = submitOrderVo.getLinkIds().split(",");//相关联系人id
        //订单购买数量
        order.setTotalCount((long) linkIds.length);
        for (int i = 0; i < XYs.length; i++) {
            //初始化订单联系人表
            OrderLinkUser orderLinkUser = new OrderLinkUser();
            orderLinkUser.setLinkUserId(Long.parseLong(linkIds[i]));//联系人id
            //根据linkIds[i]  查询dm_link_user 入住人姓名
            LinkUser linkUser = linkUserFegin.getOneLinkUser(Long.valueOf(linkIds[i]), Long.valueOf(uId));
            orderLinkUser.setLinkUserName(linkUser.getName());
            //在分割成  X  Y
            Long X = 0L, Y = 0L;
            String[] XY = XYs[i].split("_");
            X = Long.parseLong(XY[0]);//单个X
            Y = Long.parseLong(XY[1]);//单个Y
            orderLinkUser.setX(X);
            orderLinkUser.setY(Y);
            //单个价格需要去座位表中通过 排期id 和X Y 查询座位等级，根据座位等级去座位价格表中通过等级和排期id 获取座位价格
            //主要是获取 座位等级
            SchedulerSeat schedulerSeat = schedulerSeatFeign.getOneSchedulerSeat(submitOrderVo.getSchedulerId(), X, Y);
            schedulerSeat.setOrderNo(orderNo);
            schedulerSeat.setUserId(Long.valueOf(uId));
            //根据排期id  和 座位等级查询出 来  座位的价格 然后填充 订单联系人表
            SchedulerSeatPrice schedulerSeatPrice = schedulerSeatPriceFeign.getOneSchedulerSeatPrice(schedulerSeat.getScheduleId(), schedulerSeat.getAreaLevel());
            //单个座位价格进行累加 订单价格
            orderPrice += schedulerSeatPrice.getPrice();
            //填充单个座位价格参数
            orderLinkUser.setPrice(schedulerSeatPrice.getPrice());
            //填充订单号
            orderLinkUser.setOrderId(Long.parseLong(orderNo));
            //调用orderLinkUser 根据循环增加每一条订单联系人信息
            try {
                orderLinkUserService.save(orderLinkUser);
                //int i1 = 1/0;
            } catch (Exception e) {
                rabbitTemplate.convertAndSend("dm_ex_rollback", "linkuser", orderNo);
                throw new DmException(CodeEnum.ORDER_VCODE_ERROR);
            }
            //用来监听异常 如果抛异常
            try {
                //根据排期id和X轴和y轴 修改排期座位表状态和userid 和orderNO
                schedulerSeatFeign.UpOneSchedulerSeat(schedulerSeat);
            } catch (Exception e) {
                //rabbitTemplate.convertAndSend("linkuser_rollback",orderNo);
                rabbitTemplate.convertAndSend("dm_ex_rollback", "seat", orderNo);
                throw new DmException(CodeEnum.ORDER_VCODE_ERROR);
            }
        }
        //判断是否需要保险
        if (submitOrderVo.getIsNeedInsurance() == 1) yesInsurance = 20d;
        order.setTotalAmount(orderPrice + yesInsurance);//根据剧场和座位查询票价进行算法
        order.setUserId(Long.valueOf(uId));//下单用户id
        order.setInsuranceAmount(yesInsurance);//保险金额
        order.setInvoiceHead(submitOrderVo.getInvoiceHead());//发票抬头
        order.setInvoiceNo(submitOrderVo.getInvoiceNo());//发票号
        order.setOrderType(0L);
        order.setPayType("2");
        System.out.println(order);
        //调用ordermapper新增方法 进行新增一条订单
        // TODO 订单新增
        try {
            this.save(order);
        } catch (Exception e) {
            //rabbitTemplate.convertAndSend("order_rollback",orderNo);
            rabbitTemplate.convertAndSend("dm_ex_rollback", "order", orderNo);
            throw new DmException(CodeEnum.ORDER_VCODE_ERROR);
        }
        //将生成订单的消息加入消息队列当中
        rabbitTemplate.convertAndSend(myRabbitListener.getExchange(), myRabbitListener.getRoutingkey(), orderNo
                , message -> {//getDeliveryMode() 代表的是持久化数据
                    message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                    //超期时间  默认时间单位是毫秒   半小时过期
                    message.getMessageProperties().setExpiration("1800000");
                    return message;
                }
        );
        //定义方法结束时间
        long endTime = System.currentTimeMillis();
        //释放锁
        redisTemplate.delete("lock");
        System.out.println("方法从始终 消耗时间为");
        System.out.println(endTime - startTime);
        System.out.println("订单消息发送成功");
        return orderNo;
    }

    @Override
    public ConfirmPayDto getOrderPay(Long orderNo, Long uId) {
        ConfirmPayDto confirmPayDto = new ConfirmPayDto();
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq("orderNo", orderNo);
        orderQueryWrapper.eq("userId", uId);
        Order order = getOne(orderQueryWrapper);
        BeanUtils.copyProperties(order, confirmPayDto);
        confirmPayDto.setPayAmount(order.getTotalAmount());
        confirmPayDto.setTotalAmount(order.getTotalAmount() - order.getInsuranceAmount());
        System.out.println(confirmPayDto);
        return confirmPayDto;
    }

    /**
     * 删除订单
     *
     * @param OrderNo
     * @return
     */
    @Override
    public boolean DelOrder(String OrderNo) {
        UpdateWrapper<Order> orderUpdateWrapper = new UpdateWrapper<>();
        orderUpdateWrapper.eq("orderNo", OrderNo);
        return this.remove(orderUpdateWrapper);
    }

    /**
     * 根据用户id  获取订单信息
     *
     * @param param
     * @param userId
     * @return
     */
    @Override
    public List<OrdersDto> queryorderlist(Map<String, String> param, Long userId) {
        List<Order> orders = baseMapper.likeOrder(param.get("orderType"), param.get("orderTime"), param.get("keyword"), userId);
        List<OrdersDto> ordersDtos = new ArrayList<>();
        orders.forEach(order -> {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(order, ordersDto);
            ordersDto.setNum(order.getTotalCount());
            ordersDto.setSellTime(order.getCreatedTime());
            ordersDto.setUnitPrice(order.getTotalAmount());
            ordersDto.setTotalAmount(order.getTotalAmount());
            LocalDateTime dateTime = ordersDto.getSellTime();
            if (order.getOrderType() == 0) {
                ordersDto.setOrderType("待支付");
            }
            if (order.getOrderType() == -1) {
                ordersDto.setOrderType("支付超时");
            }
            if (order.getOrderType() == 2) {
                ordersDto.setOrderType("已支付");
            }
            if (order.getCreatedTime()!= null) {
                dateTime = dateTime.plusMinutes(30);
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH : mm: ss");
                String endTime = df.format(dateTime);
                ordersDto.setEndTime(endTime);
                ordersDtos.add(ordersDto);
            }
        });
        return ordersDtos;
    }
}