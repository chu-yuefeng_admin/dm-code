package com.hdax.dm.order.fegin;

import com.hdax.dm.api.SchedulerSeatControllerApi;
import com.hdax.dm.api.SchedulerSeatPriceControllerApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * 远程调用排期座位信息信息
 */
@FeignClient(name="dm-scheduler")
@Component
public interface SchedulerSeatFeign extends SchedulerSeatControllerApi {
}
