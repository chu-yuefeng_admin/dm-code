package com.hdax.dm.order.controller;

import com.hdax.dm.MainApplication;
import com.hdax.dm.common.exception.DmException;
import com.hdax.dm.order.dto.ConfirmPayDto;
import com.hdax.dm.order.dto.OrdersDto;
import com.hdax.dm.order.service.SubmitOrderService;
import com.hdax.dm.order.vo.QueryorderlistVo;
import com.hdax.dm.order.vo.SubmitOrderVo;
import com.hdax.dm.token.TokenUtil;
import com.hdax.dm.utils.CommonResponse;
import com.hdax.dm.utils.ResponseUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class SubmitOrderController {
    @Autowired
    private SubmitOrderService submitOrderService;
    //认证自定义工具类
    @Autowired
    private TokenUtil tokenUtil;
    /**
     * 根据前端vo参数 进行订单生成
     * @return
     */
    @PostMapping(path = "p/submitorder")
    public CommonResponse<String> SubmitOrder(@RequestBody SubmitOrderVo submitOrderVo, HttpServletRequest request) throws DmException {
        String token = request.getHeader("token");
        System.out.println(token);
        Claims claims = tokenUtil.token(token);
        String orderNo =  submitOrderService.SubimtOrder(Integer.parseInt(claims.getId()),submitOrderVo);
        return ResponseUtil.returnSuccess(orderNo);
    }

    /**
     *根据订单号查询订单详情
     * @param param
     * @param request
     * @return
     */
    @PostMapping(path = "p/pay/confirmpay")
    public CommonResponse<ConfirmPayDto> getOrderPay(@RequestBody Map<String ,Long> param,HttpServletRequest request){
        String token = request.getHeader("token");
        Claims claims = tokenUtil.token(token);
        return ResponseUtil.returnSuccess(submitOrderService.getOrderPay(param.get("orderNo"), Long.valueOf(claims.getId())));
    }

    /**
     * 根据 用户id  获取 订单信息
     * @param request
     * @return
     */
    @PostMapping(path = "p/queryorderlist")
    public CommonResponse<List<OrdersDto>> queryorderlist(@RequestBody Map<String,String> param, HttpServletRequest request){
        String token = request.getHeader("token");
        Claims claims = tokenUtil.token(token);
        return  ResponseUtil.returnSuccess(submitOrderService.queryorderlist(param,Long.valueOf(claims.getId())));
    }
}
