package com.hdax.dm.order.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
///个人中心订单信息
public class OrdersDto {
    private String orderNo;//订单编号
    private String orderType;
    private LocalDateTime sellTime;// 购买时间createdTime
    private String itemName;//商品名称
    private Long num;//购买数量totalCount
    private Double unitPrice;//商品单价
    private Double totalAmount;//总价
    private String  endTime;
}
