package com.hdax.dm.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdax.dm.entity.order.OrderLinkUser;

public interface OrderLinkUserService extends IService<OrderLinkUser> {
    /**
     * 根据 订单编号删除 订单联系人信息
     * @param OrderNo
     * @return
     */
    boolean DelOrderLink(String OrderNo);
}
