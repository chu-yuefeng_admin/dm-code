package com.hdax.dm.order.mq;

import com.hdax.dm.entity.order.Order;
import com.hdax.dm.order.fegin.SchedulerSeatFeign;
import com.hdax.dm.order.mappers.OrderMapper;
import com.hdax.dm.order.service.OrderLinkUserService;
import com.hdax.dm.order.service.SubmitOrderService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@RabbitListener(queues = {"dm_dlx_queue"})
@Component
public class MyRabbitListener {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private SchedulerSeatFeign schedulerSeatFeign;
    //用来 订单 延时 消息  让 失效的消息 进入到 死信队列 然后死信队列监听到 进行消费
    @RabbitHandler//@Payload
    public void consumer(Channel channel, Message message, @Payload String orderNo) throws IOException {
        String queue =message.getMessageProperties().getConsumerQueue();//消息队列名
        long tag = message.getMessageProperties().getDeliveryTag();
        System.out.println("订单号");
        System.out.println(orderNo);
        //查看订单的支付状态
        Order order = orderMapper.selectOne(orderNo);
        channel.basicAck(tag,false);
        if(order.getOrderType() == 0){
            //对数据进行回滚   让座位变为原始状态 并且修改订单状态为  -1
            int flag1 = orderMapper.OrderTimedOutState(orderNo);
            boolean flag2 = schedulerSeatFeign.UpTwoSchedulerSeat(orderNo);
            System.out.println(flag1);
            System.out.println(flag2);
            if(flag1>1&&flag2){
                channel.basicAck(tag,false);
            }else{
                //重试
                channel.basicNack(tag,false,false);
            }
        }else{
            channel.basicAck(tag,false);
            return;
        }
    }
}
