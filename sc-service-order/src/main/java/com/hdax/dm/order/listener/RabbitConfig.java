package com.hdax.dm.order.listener;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitConfig {

    @Value("${dm.order.exchange}")
    private String exchange;
    @Value("${dm.order.routingkey}")
    private String routingkey;
    @Value("${dm.order.queue}")
    private String queue;
    @Value("${dm.order.dlx.exchange}")
    private String dlxExchange;
    @Value("${dm.order.dlx.routingkey}")
    private String dlxRoutingkey;
    @Value("${dm.order.dlx.queue}")
    private String dlxQueue;

    public String getExchange() {
        return exchange;
    }

    public String getRoutingkey() {
        return routingkey;
    }

    public String getQueue() {
        return queue;
    }

    public String getDlxExchange() {
        return dlxExchange;
    }

    public String getDlxRoutingkey() {
        return dlxRoutingkey;
    }

    public String getDlxQueue() {
        return dlxQueue;
    }

    //Bean
    //普通队列交换器
    @Bean
    public DirectExchange exchange(){
        return new DirectExchange(exchange);
    }
    //普通队列
    @Bean
    public Queue queue(){
        Map<String,Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange",dlxExchange);
        args.put("x-dead-letter-routing-key",dlxRoutingkey);
        return new Queue(queue,true,false,false,args);
    }

    //队列路由绑定
    @Bean
    public Binding binding(){
        return BindingBuilder.
                bind(queue())
                .to(exchange())
                .with(routingkey);
    }


    //死信队列交换器
    @Bean
    public DirectExchange dlxExchange(){
        return new DirectExchange(dlxExchange);
    }
    //死信队列
    @Bean
    public Queue dlxQueue(){
        return new Queue(dlxQueue);
    }

    //死信队列路由绑定
    @Bean
    public Binding dlxBinding(){
        return BindingBuilder.
                bind(dlxQueue())
                .to(dlxExchange())
                .with(dlxRoutingkey);
    }
}