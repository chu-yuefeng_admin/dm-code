package com.hdax.dm.order.vo;

import lombok.Data;

@Data
//用来接收个人中心的订单查询条件
public class QueryorderlistVo {
    private String keyword;//订单号或者商品名称
    private Long orderTime;//下单时间  一年内 1  近三个月 2
    private Long orderType;//订单类型  0 -1 2
}
